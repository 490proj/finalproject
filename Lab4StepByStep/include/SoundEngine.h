
#include "Balls.h"
#include "UserDetail.h"
#include <irrklang/irrKlang.h>
#pragma comment(lib, "irrKlang.lib") // link with irrKlang.dll
#include <random>
#include <stdio.h>   
#include <stdlib.h>     
#include <time.h>       

#ifndef SOUND
#define SOUND
using namespace irrklang;
#define SE_SongsCount 7	//Arbitrarily chosen number of tracks
#define SE_stepVars 4 //Arbitrarily chosen number of step sound variations
#define rollSoundLength 4.0
#define minDist 5.0

class SoundEngine {
private:
	//The sound engine
	ISoundEngine * engine;
	BallCollection* m_B;
	Users* m_U;

	//The global variables
	float SE_MVol = 1.00;	//Master Volume 
	float SE_JukeVol = 1.00;	//Jukebox volume 
	int SE_index_songsList;	//Stores the index of the next song in songsList 
	int SE_index_stepVars = 0;	//Stores the index of the current step variation being played 
	vec3df sndLoc;	//The 3D vector for the position of the 3D sounds 
	float ball_Strength;
	int timer_ball1;
	int timer_ball2;
	int timer_ball3;
	int timer_wall1;
	int timer_wall2;
	int timer_wall3;

	bool SE_startUp = true;	//Boolean that tells whether this is the first run through or not (initiation step is different) 
	int SE_player_stepCount = 0;	//Tracker for the number of steps taken (allows more control) 
	ik_u32 playPos;	//The position in the playback (in ms) 

					//The songs
	ISound* current_track;	//The track currently being played 
	const ik_c8* songsList[SE_SongsCount];	//[ik_c8] array of songs names that the engine can call 
	ISoundSource* track0;	//[ISoundSource] 1st track in the list 
	ISoundSource* track1;	//[ISoundSource] 2nd track in the list 
	ISoundSource* track2;	//[ISoundSource] 3rd track in the list 
	ISoundSource* track3;	//[ISoundSource] 4th track in the list 
	ISoundSource* track4;	//[ISoundSource] 5th track in the list
	ISoundSource* track5;	//[ISoundSource] 6th track in the list 
	ISoundSource* track6;	//[ISoundSource] 7th track in the list 

							//Ball Sounds 
	ISoundSource* ball_Ball;	//[ISoundSource] Collision sound of 2 balls 
	ISound* ball_Ball_col1;	// 3 ISounds created to limit the amount of  
	ISound* ball_Ball_col2;	// sounds that can be called at the same time 
	ISound* ball_Ball_col3;	// (due to: physics errors overload engine) 
	ISoundSource* ball_Wall;	//[ISoundSource] Collision sound of a ball hitting a wall 
	ISound* ball_Wall_col1;	// 2 ISounds created to limit the amount of 
	ISound* ball_Wall_col2;	// sounds that can be called at the same time 
	ISound* ball_Wall_col3;	// (due to: physics erros overload the engine) 
	ISoundSource* ball_Pole;	//[ISoundSource] Collision sound of the pole on the ball 
	ISoundSource* ball_Hole;	//[ISoundSource] Sound of a ball entering the pouch 
	ISoundSource* ball_R2M;	//[ISoundSource] Sound of a ball inside the table going from the pouch to the tray 

	ISound* ball_Roll_Snd;	//The track currently being played 
	ISoundSource* ball_Roll;	//[ISoundSource] temp* Sound of a ball rolling. Will eventually be generated 
	ISoundSource* ball_Pitch;	//[ISoundSource] temp* Sound of a ball spinning. Will eventually be generated (if trick shots supported) 

								//Player Sounds
	const ik_c8* stepSounds[SE_stepVars];	//[ik_c8] array of songs names that the engine can call 
	ISound* player_Step;	//[ISound] Sound at the player's footstepISoundSource* track0;	//[ISoundSource] 1st track in the list 
	ISoundSource* stepVar0;	//[ISoundSource] 1st step sound variation 
	ISoundSource* stepVar1;	//[ISoundSource] 2nd step sound variation 
	ISoundSource* stepVar2;	//[ISoundSource] 3rd step sound variation 
	ISoundSource* stepVar3;	//[ISoundSource] 4th step sound variation 
	ISoundSource* player_ScoreG;	//[ISoundSource] Sound played when a player scores for his team 
	ISoundSource* player_ScoreB;	//[ISoundSource] Sound played when a player scores for the opposing team 
	ISoundSource* player_Win;	//[ISoundSource] Sound played when a player's team wins the game 
	ISoundSource* player_Lose;	//[ISoundSource] Sound played when a player's team losses the game 

	ISound* PAUSE;	//Son de 0.45 secondes utilis� pour faire attendre le compteur de pas


public:


	//Methods:
	SoundEngine(BallCollection* B, Users* U);	//Constructor
	~SoundEngine();	//Deconstructor

	bool SoundLoad();	//Init(). Returns TRUE if sounds and engine were loaded in successfully 
	void keyControls(unsigned char key);	//The function called when a key is hit 
	void update(vec4 position, vec3 lookDirection);	// Mets � jour la position de la cam�ra pour le moteur de sons 
	void SoundEvents(vec3 Position, int SE_sound_event);	//The main of the SoundEngine; where the events are handled and the sound is played
															/**
															* case 0: Player events
															* case 1: When 2 balls come in contact with each other
															* case 2: When a ball hits a wall
															* case 3: When the pole strikes the ball
															* case 4: When the ball enters a pouch
															* case 5: When the ball rolls
															* case 6: When the ball rotates (curve ball)
															* case 7: When the player moves [called by keyControls()]
															*/

															
	//Functions to translate vectors to irrklang vectors 
	vec3df vec3_df(vec3 vector) {
		return vec3df(vector.x, vector.y, vector.z);
	}
	vec3df vec3_df(vec4 vector) {
		return vec3df(vector.x, vector.y, vector.z);
	}
	float getNorm(vec4 vector) {
		return sqrt((vector.x * vector.x) + (vector.y * vector.y) + (vector.z * vector.z));
	}
};

#endif