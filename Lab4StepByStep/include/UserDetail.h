
#include <Angel.h>
#define MOVEMENT_SPEED 0.2f

#ifndef USERDETAIL
#define USERDETAIL

struct UserDetail
{
	vec4 camera_pos;
	float camera_angle_yaw;
	float camera_angle_pitch;
	float movement_speed;
	bool crouch;
};

class Users
{
private:
	UserDetail m_Users[2];
	int m_currentUser;
public:
	Users(vec4 cameraPos1, vec4 cameraPos2, float camera_angle_yaw1, float camera_angle_yaw2);
	UserDetail* operator[](int index);
	int getCurrentUser();
	void setCurrentUser(int newUser);
	void switchU ();
	void switchCurrentUser();
	vec4 toggleCrouch();
	float getMovSpeed();
	vec4 getCameraPos();
	float getCameraAngleYaw();
	float getCameraAnglePitch();
	void setCameraPos(vec4 entry);
	void setCameraAngleYaw(float entry);
	void setCameraAnglePitch(float entry);
};

#endif