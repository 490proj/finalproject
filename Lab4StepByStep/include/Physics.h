
#include "Balls.h"
#include "SoundEngine.h"

struct vec8 {

	GLfloat  x;
	GLfloat  y;
	GLfloat  z;
	GLfloat  w;
	GLfloat  a;
	GLfloat  b;
	GLfloat  c;
	GLfloat  d;

	//
	//  --- Constructors and Destructors ---
	//

	vec8(GLfloat s = GLfloat(0.0)) :
		x(s), y(s), z(s), w(s), a(s), b(s), c(s), d(s) {}

	vec8(GLfloat x, GLfloat y, GLfloat z, GLfloat w, GLfloat a, GLfloat b, GLfloat c, GLfloat d) :
		x(x), y(y), z(z), w(w), a(a), b(b), c(c), d(d) {}

	vec8(const vec4& v, vec4& u) { x = v.x;  y = v.y;  z = v.z;  w = v.w; a = u.x;  b = u.y;  c = u.z;  d = u.w; }

};

class Physics
{
private:

	//Variables
	int Master[16][16];
	vec8 MasterCollision[16][16];

	//Math functions:

	BallCollection* m_b;
	SoundEngine* m_s;

	vec4 vectorSubtraction(const vec4 &u, const vec4 &v);

public:

	//Constructor
	Physics(BallCollection* b, SoundEngine* s);

	//Destructor
	~Physics();

	/*
	What I need to know from the balls:
	- Position of the center of the ball
	- Position of the camera
	- Radius of the ball
	- Ball velocity
	- Key press between 1 - 5
	*/


	//Physics methods

	void CollisionBallVsWall(int i);

	bool PocketCheck(const vec4 &ballPos);

	bool CheckIfBallIsMoving(const vec4 &ballVelocity);

	bool MovementCheck();

	void Sink(const int number);

	vec4 Friction(const vec4 &ballVelocity);

	vec4 BallMovement(const vec4 &ballPos, const vec4 &ballVelocity);

	vec4 TurnBall(const vec4 &ballRot, const vec4 &ballVel);

	void SetStrength(const char &key);

	vec4 BallInitialMovement(const vec4 &cameraPos, const vec4 &ballPos);

	bool DoCirclesOverlap(int ball1, int ball2);

	void update();

};