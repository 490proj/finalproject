

#include <Angel.h>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "Balls.h"
#include "UserDetail.h"

//Scene class, as is, not really made to make subclasses
class Scene
{
private:
	//******** Shaders
	GLuint ColorShaderID;
	GLuint ShadowShaderID;
	GLuint TextureShaderID;
	//Color Uniform Location
	GLuint ColorUnifLoc;
	//Texture Unit Uniform Location
	GLuint TexUnitUnifLoc;
	//Textures
	GLuint EnviTexID;
	GLuint CueTexID;
	GLuint SurfaceTexID;
	GLuint PlayerTexID[2];
	GLuint BallTexture[16];

	//******** Grid data here
	//Grid VAO (vertext array object)
	GLuint LightsObject;
	//Grid Color
	vec4 LightsColor;
	//Grid line count
	int LightsCount;


	//***************************needed for blender import************************//
	GLuint EnviObject;
	GLuint BallObject;
	GLuint CueObject;
	GLuint PlayerObject;
	GLuint SurfaceObject;

	vec4 EnviPosition;
	vec4 CuePosition;
	vec4 PlayerPosition;
	vec4 BallPosition;
	vec4 BallRotation;

	vec4* EnviVertBuf;
	vec4* BallVertBuf;
	vec4* CueVertBuf;
	vec4* PlayerVertBuf;
	vec4* SurfaceVertBuf;

	vec4* EnviNormBuf;
	vec4* BallNormBuf;
	vec4* CueNormBuf;
	vec4* PlayerNormBuf;
	vec4* SurfaceNormBuf;

	vec2* EnviTextBuf;
	vec2* BallTextBuf;
	vec2* CueTextBuf;
	vec2* PlayerTextBuf;
	vec2* SurfaceTextBuf;

	int triangle_count_envi = 0;
	int triangle_count_ball = 0;
	int triangle_count_cue = 0;
	int triangle_count_player = 0;
	int triangle_count_surface = 0;

	const char* Cue_Name = "..\\Assets\\cuecolors.bmp";
	const char* Surface_Name = "..\\Assets\\surfaceDeJeu.bmp";
	const char* Player1_Name = "..\\Assets\\player1.bmp";
	const char* Player2_Name = "..\\Assets\\player2.bmp";

	const char* Envi_Name = "..\\Assets\\envi_texture2.bmp";
	const char* ballTextureLoc[16] = { "..\\Assets\\0.bmp",  "..\\Assets\\1.bmp",  "..\\Assets\\2.bmp",  "..\\Assets\\3.bmp",
		"..\\Assets\\4.bmp",  "..\\Assets\\5.bmp",  "..\\Assets\\6.bmp",  "..\\Assets\\7.bmp",
		"..\\Assets\\8.bmp",  "..\\Assets\\9.bmp",  "..\\Assets\\10.bmp", "..\\Assets\\11.bmp",
		"..\\Assets\\12.bmp", "..\\Assets\\13.bmp", "..\\Assets\\14.bmp", "..\\Assets\\15.bmp" };

	//******** Light information here **************
	GLuint LightShaderID;
	GLuint TexUnitLoc;
	GLuint ViewMatrixLoc;
	GLuint ObjMatrixLoc;
	GLuint NormMatrixLoc;
	GLuint AmbientColorLoc;

	vec4 AmbientColor;

	GLuint LightPosition1Loc;
	vec4 LightPosition1;
	GLuint LightColor1Loc;
	vec4 LightColor1;
	GLuint LightHeading1Loc;
	vec4 LightHeading1;

	GLuint LightPosition2Loc;
	vec4 LightPosition2;
	GLuint LightColor2Loc;
	vec4 LightColor2;
	GLuint LightHeading2Loc;
	vec4 LightHeading2;

	GLuint LightPosition3Loc;
	vec4 LightPosition3;
	GLuint LightColor3Loc;
	vec4 LightColor3;
	GLuint LightHeading3Loc;
	vec4 LightHeading3;

	GLuint CameraPositionLoc;


	GLuint BallsLoc[16];
	vec4 ballPosArray[16];
	GLuint calcShadow;
	int calcShadowLoc;

	GLuint specularReflection;
	int specularReflectionLoc;


public:
	//Constructor
	Scene();
	//Destructor
	~Scene();

	//Methods
	void LoadResource();
	void UnloadResource();

	void GetEnviObject();
	void GetBallObject();
	void GetCueObject();
	void GetPlayerObject();
	void GetSurfaceObject();

	void LoadScene(BallCollection* balls, Users* users);
	void UnloadScene();

	void OnDraw();

	void ResetUserPosition();
};