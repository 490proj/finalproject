
#include <Angel.h>
#define BALL_RADIUS 0.0875f
#define WHITEBALL 0


#ifndef BALL
#define BALL

struct Ball
{
	int ballNumber;  // Between 0 and 15
	vec4 position;   // Position of the center of the ball (X,Y,Z)
	vec4 velocity;   // Direction and magnitude of the direction of the ball (X,Y,Z)
	vec4 rotation;   // Orientation of the ball (X,Y,Z)
	bool onTable;    // Used to test if ball is on table or not (mostly used for the physics)
    //SPIN
};

class BallCollection
{
private:
	Ball m_Balls[16];

public:
	BallCollection();
	void Reset();
	Ball* operator[](int index);
	vec4 getBallPosition(int number);
	vec4 getBallVelocity(int number);
	vec4 getBallRotation(int number);
	void setBallPosition(int number, vec4 newPos);
	void setBallVelocity(int number, vec4 newVel);
	void setBallRotation(int number, vec4 newRot);
	void trueOnTable(int number);
	void falseOnTable(int number);
	bool returnOnTable(int number);
};

#endif // !BALL

