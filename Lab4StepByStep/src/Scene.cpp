

#include "Scene.h"
#include "loadBMP.h"


BallCollection* SceneBalls;
Users* SceneUsers;


//Class constructor
Scene::Scene()
{
}


//Class destructor
Scene::~Scene()
{
}


//Loading all ressources here
void Scene::LoadResource()
{

	//Loading color shaders
	std::cout << "Loading..." << "ColorShader" << std::endl;
	ColorShaderID = InitShader("src\\ColorVertexShader.glsl", "src\\ColorFragmentShader.glsl");
	std::cout << "ColorShader ID = " << ColorShaderID << std::endl;



	//Get color uniform location
	ColorUnifLoc = glGetUniformLocation(ColorShaderID, "vColor");

	//Loading texture shaders
	std::cout << "Loading..." << "TextureShader" << std::endl;
	TextureShaderID = InitShader("src\\TextureVertexShader.glsl", "src\\TextureFragmentShader.glsl");
	std::cout << "TextureShader ID = " << TextureShaderID << std::endl;

	//Get texture unit location
	TexUnitUnifLoc = glGetUniformLocation(TextureShaderID, "sTexUnit");


	//*****************************************************************************************************************************************************************
	//*****************************************************************************************************************************************************************
	//*****************************************************************************************************************************************************************
	//For the balls' textures, we should have an array of the 16 balls' textures with 0 being white, 1 being the number 1 ball and so on, this will help for the onDraw
	//*****************************************************************************************************************************************************************
	//*****************************************************************************************************************************************************************
	//*****************************************************************************************************************************************************************



	//Loading textures

	EnviTexID = LoadTexFromBMP(Envi_Name);
	CueTexID = LoadTexFromBMP(Cue_Name);
	SurfaceTexID = LoadTexFromBMP(Surface_Name);
	PlayerTexID[0] = LoadTexFromBMP(Player1_Name);
	PlayerTexID[1] = LoadTexFromBMP(Player2_Name);

	for (int i = 0; i < 16; i++)
	{
		const char* Ball_Name = ballTextureLoc[i];
		BallTexture[i] = LoadTexFromBMP(Ball_Name);
	}

	//Light Shader stuff here *********************************




	//Loading light shaders
	std::cout << "Loading..." << "LightShader" << std::endl;
	LightShaderID = InitShader("src\\LightVertexShader.glsl", "src\\LightFragmentShader.glsl");
	std::cout << "LightShader ID = " << LightShaderID << std::endl;

	//Get texture unit location
	TexUnitLoc = glGetUniformLocation(LightShaderID, "texID");
	//Get the view tranformaiton matrix location
	ViewMatrixLoc = glGetUniformLocation(LightShaderID, "viewMat");
	ObjMatrixLoc = glGetUniformLocation(LightShaderID, "objMat");
	NormMatrixLoc = glGetUniformLocation(LightShaderID, "normMat");
	//Get ambient color location
	AmbientColorLoc = glGetUniformLocation(LightShaderID, "vAmbientColor");
	//Get the light position and color locations
	LightPosition1Loc = glGetUniformLocation(LightShaderID, "vLightPosition1");
	LightColor1Loc = glGetUniformLocation(LightShaderID, "vLightColor1");
	LightHeading1Loc = glGetUniformLocation(LightShaderID, "vLightHeading1");

	LightPosition2Loc = glGetUniformLocation(LightShaderID, "vLightPosition2");
	LightColor2Loc = glGetUniformLocation(LightShaderID, "vLightColor2");
	LightHeading2Loc = glGetUniformLocation(LightShaderID, "vLightHeading2");

	LightPosition3Loc = glGetUniformLocation(LightShaderID, "vLightPosition3");
	LightColor3Loc = glGetUniformLocation(LightShaderID, "vLightColor3");
	LightHeading3Loc = glGetUniformLocation(LightShaderID, "vLightHeading3");

	//Camera position location (for specularity)
	CameraPositionLoc = glGetUniformLocation(LightShaderID, "vCameraPosition");
	BallsLoc[0] = glGetUniformLocation(LightShaderID, "vBallPos");
	calcShadow = glGetUniformLocation(LightShaderID, "vcalcShadow");
	specularReflection = glGetUniformLocation(LightShaderID, "cshowReflections");
}


void Scene::GetEnviObject() {
	using namespace std;
	std::cout << "ENETERING ENVI READER" << std::endl;



	vec4* tempVertArray;
	vec4* tempNormArray;
	vec2* tempTextArray;

	int* VertexOrder;
	int* TextureOrder;
	int* NormalOrder;

	int vertex_count = 0;
	int normal_count = 0;
	int texture_count = 0;


	char reader[1000];
	int vertexCursor = 0;
	int textureCursor = 0;
	int normalCursor = 0;
	int triOrderCursor = 0;
	char tempString[20];
	float tempX;
	float tempY;
	float tempZ;
	int intVertex;
	int intTexture;
	int intNormal;
	int number_of_spaces = 0;
	int len = 0;
	char tempString1[20];
	char tempString2[20];
	char tempString3[20];
	char tempString4[20];
	char tempChar;


	//first time to count
	ifstream myfile;
	myfile.open("..\\Assets\\table_walls_precombine.obj");

	do {
		myfile.getline(reader, 1000);
		//std::cout << reader << std::endl;
		if (reader[0] == 'v' & reader[1] == ' ')
		{
			vertex_count++;
		}
		else if (reader[0] == 'v' & reader[1] == 't')
		{
			texture_count++;
		}
		else if (reader[0] == 'v' & reader[1] == 'n')
		{
			normal_count++;
		}
		else if (reader[0] == 'f')
		{
			number_of_spaces = 0;
			for (int i = 0; i < strlen(reader); i++) {
				if (reader[i] == ' ') number_of_spaces++;
			}
			if (number_of_spaces >= 3)triangle_count_envi++;
			if (number_of_spaces >= 4)triangle_count_envi++;
		}
	} while (myfile.peek() != EOF);

	tempVertArray = new vec4[vertex_count];
	tempNormArray = new vec4[normal_count];
	tempTextArray = new vec2[texture_count];
	VertexOrder = new int[triangle_count_envi * 3];
	TextureOrder = new int[triangle_count_envi * 3];
	NormalOrder = new int[triangle_count_envi * 3];
	EnviVertBuf = new vec4[triangle_count_envi * 3];
	EnviNormBuf = new vec4[triangle_count_envi * 3];
	EnviTextBuf = new vec2[triangle_count_envi * 3];
	//second time to fill the arrays
	myfile.close();
	myfile.open("..\\Assets\\table_walls_precombine.obj");
	do {
		myfile.getline(reader, 1000);


		if (reader[0] == 'v' & reader[1] == ' ')
		{

			sscanf(reader, "%s %f %f %f", tempString, &tempX, &tempY, &tempZ);
			tempVertArray[vertexCursor] = vec4(tempX, tempY, tempZ, 1.0f);
			vertexCursor++;
		}

		else if (reader[0] == 'v' & reader[1] == 't')
		{

			sscanf(reader, "%s %f %f", tempString, &tempX, &tempY);
			tempTextArray[textureCursor] = vec2(tempX, tempY);
			textureCursor++;
		}

		else if (reader[0] == 'v' & reader[1] == 'n')
		{

			sscanf(reader, "%s %f %f %f", tempString, &tempX, &tempY, &tempZ);
			tempNormArray[normalCursor] = vec4(tempX, tempY, tempZ, 0.0f);
			normalCursor++;
		}

		else if (reader[0] == 'f')
		{
			number_of_spaces = 0;
			for (int i = 0; i < strlen(reader); i++) {
				if (reader[i] == ' ') number_of_spaces++;
			}

			if (number_of_spaces == 3) {
				//std::cout << number_of_spaces << std::endl;

				sscanf(reader, "%s %s %s %s", tempString, tempString1, tempString2, tempString3);

				sscanf(tempString1, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString2, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString3, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;
			}
			else if (number_of_spaces = 4) {
				sscanf(reader, "%s %s %s %s %s", tempString, tempString1, tempString2, tempString3, tempString4);

				sscanf(tempString1, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString2, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString3, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString4, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString3, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString1, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;
			}

		}
	} while (myfile.peek() != EOF);

	//create buffer material/////////////////////////////////
	vertexCursor = 0;
	std::cout << triangle_count_envi * 3 << std::endl;
	while (vertexCursor < triangle_count_envi * 3) {
		EnviVertBuf[vertexCursor] = tempVertArray[VertexOrder[vertexCursor]];
		EnviTextBuf[vertexCursor] = tempTextArray[TextureOrder[vertexCursor]];
		EnviNormBuf[vertexCursor] = tempNormArray[NormalOrder[vertexCursor]];
		//std::cout << BlenderNormBuf[vertexCursor] << std::endl;
		vertexCursor++;
	}
	std::cout << "FINISHED LOADING" << std::endl;
}

void Scene::GetSurfaceObject() {
	using namespace std;
	std::cout << "ENETERING SURFACE READER" << std::endl;



	vec4* tempVertArray;
	vec4* tempNormArray;
	vec2* tempTextArray;

	int* VertexOrder;
	int* TextureOrder;
	int* NormalOrder;

	int vertex_count = 0;
	int normal_count = 0;
	int texture_count = 0;


	char reader[1000];
	int vertexCursor = 0;
	int textureCursor = 0;
	int normalCursor = 0;
	int triOrderCursor = 0;
	char tempString[20];
	float tempX;
	float tempY;
	float tempZ;
	int intVertex;
	int intTexture;
	int intNormal;
	int number_of_spaces = 0;
	int len = 0;
	char tempString1[20];
	char tempString2[20];
	char tempString3[20];
	char tempString4[20];
	char tempChar;


	//first time to count
	ifstream myfile;
	myfile.open("..\\Assets\\surface.obj");

	do {
		myfile.getline(reader, 1000);
		//std::cout << reader << std::endl;
		if (reader[0] == 'v' & reader[1] == ' ')
		{
			vertex_count++;
		}
		else if (reader[0] == 'v' & reader[1] == 't')
		{
			texture_count++;
		}
		else if (reader[0] == 'v' & reader[1] == 'n')
		{
			normal_count++;
		}
		else if (reader[0] == 'f')
		{
			number_of_spaces = 0;
			for (int i = 0; i < strlen(reader); i++) {
				if (reader[i] == ' ') number_of_spaces++;
			}
			if (number_of_spaces >= 3)triangle_count_surface++;
			if (number_of_spaces >= 4)triangle_count_surface++;
		}
	} while (myfile.peek() != EOF);

	tempVertArray = new vec4[vertex_count];
	tempNormArray = new vec4[normal_count];
	tempTextArray = new vec2[texture_count];
	VertexOrder = new int[triangle_count_surface * 3];
	TextureOrder = new int[triangle_count_surface * 3];
	NormalOrder = new int[triangle_count_surface * 3];
	SurfaceVertBuf = new vec4[triangle_count_surface * 3];
	SurfaceNormBuf = new vec4[triangle_count_surface * 3];
	SurfaceTextBuf = new vec2[triangle_count_surface * 3];
	//second time to fill the arrays
	myfile.close();
	myfile.open("..\\Assets\\surface.obj");
	do {
		myfile.getline(reader, 1000);


		if (reader[0] == 'v' & reader[1] == ' ')
		{

			sscanf(reader, "%s %f %f %f", tempString, &tempX, &tempY, &tempZ);
			tempVertArray[vertexCursor] = vec4(tempX, tempY, tempZ, 1.0f);
			vertexCursor++;
		}

		else if (reader[0] == 'v' & reader[1] == 't')
		{

			sscanf(reader, "%s %f %f", tempString, &tempX, &tempY);
			tempTextArray[textureCursor] = vec2(tempX, tempY);
			textureCursor++;
		}

		else if (reader[0] == 'v' & reader[1] == 'n')
		{

			sscanf(reader, "%s %f %f %f", tempString, &tempX, &tempY, &tempZ);
			tempNormArray[normalCursor] = vec4(tempX, tempY, tempZ, 0.0f);
			normalCursor++;
		}

		else if (reader[0] == 'f')
		{
			number_of_spaces = 0;
			for (int i = 0; i < strlen(reader); i++) {
				if (reader[i] == ' ') number_of_spaces++;
			}

			if (number_of_spaces == 3) {
				//std::cout << number_of_spaces << std::endl;

				sscanf(reader, "%s %s %s %s", tempString, tempString1, tempString2, tempString3);

				sscanf(tempString1, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString2, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString3, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;
			}
			else if (number_of_spaces = 4) {
				sscanf(reader, "%s %s %s %s %s", tempString, tempString1, tempString2, tempString3, tempString4);

				sscanf(tempString1, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString2, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString3, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString4, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString3, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString1, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;
			}

		}
	} while (myfile.peek() != EOF);

	//create buffer material/////////////////////////////////
	vertexCursor = 0;
	std::cout << triangle_count_surface * 3 << std::endl;
	while (vertexCursor < triangle_count_surface * 3) {
		SurfaceVertBuf[vertexCursor] = tempVertArray[VertexOrder[vertexCursor]];
		SurfaceTextBuf[vertexCursor] = tempTextArray[TextureOrder[vertexCursor]];
		SurfaceNormBuf[vertexCursor] = tempNormArray[NormalOrder[vertexCursor]];
		//std::cout << BlenderNormBuf[vertexCursor] << std::endl;
		vertexCursor++;
	}
	std::cout << "FINISHED LOADING" << std::endl;
}

void Scene::GetBallObject() {
	using namespace std;
	std::cout << "ENETERING Ball READER" << std::endl;



	vec4* tempVertArray;
	vec4* tempNormArray;
	vec2* tempTextArray;

	int* VertexOrder;
	int* TextureOrder;
	int* NormalOrder;

	int vertex_count = 0;
	int normal_count = 0;
	int texture_count = 0;


	char reader[1000];
	int vertexCursor = 0;
	int textureCursor = 0;
	int normalCursor = 0;
	int triOrderCursor = 0;
	char tempString[20];
	float tempX;
	float tempY;
	float tempZ;
	int intVertex;
	int intTexture;
	int intNormal;
	int number_of_spaces = 0;
	int len = 0;
	char tempString1[20];
	char tempString2[20];
	char tempString3[20];
	char tempString4[20];
	char tempChar;


	//first time to count
	ifstream myfile;
	myfile.open("..\\Assets\\ball.obj");

	do {
		myfile.getline(reader, 1000);
		//std::cout << reader << std::endl;
		if (reader[0] == 'v' & reader[1] == ' ')
		{
			vertex_count++;
		}
		else if (reader[0] == 'v' & reader[1] == 't')
		{
			texture_count++;
		}
		else if (reader[0] == 'v' & reader[1] == 'n')
		{
			normal_count++;
		}
		else if (reader[0] == 'f')
		{
			number_of_spaces = 0;
			for (int i = 0; i < strlen(reader); i++) {
				if (reader[i] == ' ') number_of_spaces++;
			}
			if (number_of_spaces >= 3)triangle_count_ball++;
			if (number_of_spaces >= 4)triangle_count_ball++;
		}
	} while (myfile.peek() != EOF);

	tempVertArray = new vec4[vertex_count];
	tempNormArray = new vec4[normal_count];
	tempTextArray = new vec2[texture_count];
	VertexOrder = new int[triangle_count_ball * 3];
	TextureOrder = new int[triangle_count_ball * 3];
	NormalOrder = new int[triangle_count_ball * 3];
	BallVertBuf = new vec4[triangle_count_ball * 3];
	BallNormBuf = new vec4[triangle_count_ball * 3];
	BallTextBuf = new vec2[triangle_count_ball * 3];
	//second time to fill the arrays
	myfile.close();
	myfile.open("..\\Assets\\ball.obj");
	do {
		myfile.getline(reader, 1000);

		if (reader[0] == 'v' & reader[1] == ' ')
		{

			sscanf(reader, "%s %f %f %f", tempString, &tempX, &tempY, &tempZ);
			tempVertArray[vertexCursor] = vec4(tempX, tempY, tempZ, 1.0f);
			vertexCursor++;
		}

		else if (reader[0] == 'v' & reader[1] == 't')
		{

			sscanf(reader, "%s %f %f", tempString, &tempX, &tempY);
			tempTextArray[textureCursor] = vec2(tempX, tempY);
			textureCursor++;
		}

		else if (reader[0] == 'v' & reader[1] == 'n')
		{

			sscanf(reader, "%s %f %f %f", tempString, &tempX, &tempY, &tempZ);
			tempNormArray[normalCursor] = vec4(tempX, tempY, tempZ, 0.0f);
			normalCursor++;
		}

		else if (reader[0] == 'f')
		{
			number_of_spaces = 0;
			for (int i = 0; i < strlen(reader); i++) {
				if (reader[i] == ' ') number_of_spaces++;
			}

			if (number_of_spaces == 3) {
				//std::cout << number_of_spaces << std::endl;

				sscanf(reader, "%s %s %s %s", tempString, tempString1, tempString2, tempString3);

				sscanf(tempString1, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString2, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString3, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;
			}
			else if (number_of_spaces = 4) {
				sscanf(reader, "%s %s %s %s %s", tempString, tempString1, tempString2, tempString3, tempString4);

				sscanf(tempString1, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString2, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString3, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString4, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString3, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString1, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;
			}

		}
	} while (myfile.peek() != EOF);

	//create buffer material/////////////////////////////////
	vertexCursor = 0;
	while (vertexCursor < triangle_count_ball * 3) {
		BallVertBuf[vertexCursor] = tempVertArray[VertexOrder[vertexCursor]];
		BallTextBuf[vertexCursor] = tempTextArray[TextureOrder[vertexCursor]];
		BallNormBuf[vertexCursor] = tempNormArray[NormalOrder[vertexCursor]];
		vertexCursor++;
	}
	std::cout << "FINISHED LOADING" << std::endl;
}

void Scene::GetCueObject() {
	using namespace std;
	std::cout << "ENETERING CUE READER" << std::endl;



	vec4* tempVertArray;
	vec4* tempNormArray;
	vec2* tempTextArray;

	int* VertexOrder;
	int* TextureOrder;
	int* NormalOrder;

	int vertex_count = 0;
	int normal_count = 0;
	int texture_count = 0;


	char reader[1000];
	int vertexCursor = 0;
	int textureCursor = 0;
	int normalCursor = 0;
	int triOrderCursor = 0;
	char tempString[20];
	float tempX;
	float tempY;
	float tempZ;
	int intVertex;
	int intTexture;
	int intNormal;
	int number_of_spaces = 0;
	int len = 0;
	char tempString1[20];
	char tempString2[20];
	char tempString3[20];
	char tempString4[20];
	char tempChar;


	//first time to count
	ifstream myfile;
	myfile.open("..\\Assets\\cue.obj");

	do {
		myfile.getline(reader, 1000);
		//std::cout << reader << std::endl;
		if (reader[0] == 'v' & reader[1] == ' ')
		{
			vertex_count++;
		}
		else if (reader[0] == 'v' & reader[1] == 't')
		{
			texture_count++;
		}
		else if (reader[0] == 'v' & reader[1] == 'n')
		{
			normal_count++;
		}
		else if (reader[0] == 'f')
		{
			number_of_spaces = 0;
			for (int i = 0; i < strlen(reader); i++) {
				if (reader[i] == ' ') number_of_spaces++;
			}
			if (number_of_spaces >= 3)triangle_count_cue++;
			if (number_of_spaces >= 4)triangle_count_cue++;
		}
	} while (myfile.peek() != EOF);

	tempVertArray = new vec4[vertex_count];
	tempNormArray = new vec4[normal_count];
	tempTextArray = new vec2[texture_count];
	VertexOrder = new int[triangle_count_cue * 3];
	TextureOrder = new int[triangle_count_cue * 3];
	NormalOrder = new int[triangle_count_cue * 3];
	CueVertBuf = new vec4[triangle_count_cue * 3];
	CueNormBuf = new vec4[triangle_count_cue * 3];
	CueTextBuf = new vec2[triangle_count_cue * 3];
	//second time to fill the arrays
	myfile.close();
	myfile.open("..\\Assets\\cue.obj");
	do {
		myfile.getline(reader, 1000);


		if (reader[0] == 'v' & reader[1] == ' ')
		{

			sscanf(reader, "%s %f %f %f", tempString, &tempX, &tempY, &tempZ);
			tempVertArray[vertexCursor] = vec4(tempX, tempY, tempZ, 1.0f);
			vertexCursor++;
		}

		else if (reader[0] == 'v' & reader[1] == 't')
		{

			sscanf(reader, "%s %f %f", tempString, &tempX, &tempY);
			tempTextArray[textureCursor] = vec2(tempX, tempY);
			textureCursor++;
		}

		else if (reader[0] == 'v' & reader[1] == 'n')
		{

			sscanf(reader, "%s %f %f %f", tempString, &tempX, &tempY, &tempZ);
			tempNormArray[normalCursor] = vec4(tempX, tempY, tempZ, 0.0f);
			normalCursor++;
		}

		else if (reader[0] == 'f')
		{
			number_of_spaces = 0;
			for (int i = 0; i < strlen(reader); i++) {
				if (reader[i] == ' ') number_of_spaces++;
			}

			if (number_of_spaces == 3) {
				//std::cout << number_of_spaces << std::endl;

				sscanf(reader, "%s %s %s %s", tempString, tempString1, tempString2, tempString3);

				sscanf(tempString1, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString2, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString3, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;
			}
			else if (number_of_spaces = 4) {
				sscanf(reader, "%s %s %s %s %s", tempString, tempString1, tempString2, tempString3, tempString4);

				sscanf(tempString1, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString2, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString3, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString4, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString3, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString1, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;
			}

		}
	} while (myfile.peek() != EOF);

	//create buffer material/////////////////////////////////
	vertexCursor = 0;
	std::cout << triangle_count_cue * 3 << std::endl;
	while (vertexCursor < triangle_count_cue * 3) {
		CueVertBuf[vertexCursor] = tempVertArray[VertexOrder[vertexCursor]];
		CueTextBuf[vertexCursor] = tempTextArray[TextureOrder[vertexCursor]];
		CueNormBuf[vertexCursor] = tempNormArray[NormalOrder[vertexCursor]];
		//std::cout << BlenderNormBuf[vertexCursor] << std::endl;
		vertexCursor++;
	}
	std::cout << "FINISHED LOADING" << std::endl;
}

void Scene::GetPlayerObject() {
	using namespace std;
	std::cout << "ENETERING PLAYER READER" << std::endl;



	vec4* tempVertArray;
	vec4* tempNormArray;
	vec2* tempTextArray;

	int* VertexOrder;
	int* TextureOrder;
	int* NormalOrder;

	int vertex_count = 0;
	int normal_count = 0;
	int texture_count = 0;


	char reader[1000];
	int vertexCursor = 0;
	int textureCursor = 0;
	int normalCursor = 0;
	int triOrderCursor = 0;
	char tempString[20];
	float tempX;
	float tempY;
	float tempZ;
	int intVertex;
	int intTexture;
	int intNormal;
	int number_of_spaces = 0;
	int len = 0;
	char tempString1[20];
	char tempString2[20];
	char tempString3[20];
	char tempString4[20];
	char tempChar;


	//first time to count
	ifstream myfile;
	myfile.open("..\\Assets\\player.obj");

	do {
		myfile.getline(reader, 1000);
		//std::cout << reader << std::endl;
		if (reader[0] == 'v' & reader[1] == ' ')
		{
			vertex_count++;
		}
		else if (reader[0] == 'v' & reader[1] == 't')
		{
			texture_count++;
		}
		else if (reader[0] == 'v' & reader[1] == 'n')
		{
			normal_count++;
		}
		else if (reader[0] == 'f')
		{
			number_of_spaces = 0;
			for (int i = 0; i < strlen(reader); i++) {
				if (reader[i] == ' ') number_of_spaces++;
			}
			if (number_of_spaces >= 3)triangle_count_player++;
			if (number_of_spaces >= 4)triangle_count_player++;
		}
	} while (myfile.peek() != EOF);

	tempVertArray = new vec4[vertex_count];
	tempNormArray = new vec4[normal_count];
	tempTextArray = new vec2[texture_count];
	VertexOrder = new int[triangle_count_player * 3];
	TextureOrder = new int[triangle_count_player * 3];
	NormalOrder = new int[triangle_count_player * 3];
	PlayerVertBuf = new vec4[triangle_count_player * 3];
	PlayerNormBuf = new vec4[triangle_count_player * 3];
	PlayerTextBuf = new vec2[triangle_count_player * 3];
	//second time to fill the arrays
	myfile.close();
	myfile.open("..\\Assets\\player.obj");
	do {
		myfile.getline(reader, 1000);


		if (reader[0] == 'v' & reader[1] == ' ')
		{

			sscanf(reader, "%s %f %f %f", tempString, &tempX, &tempY, &tempZ);
			tempVertArray[vertexCursor] = vec4(tempX, tempY, tempZ, 1.0f);
			vertexCursor++;
		}

		else if (reader[0] == 'v' & reader[1] == 't')
		{

			sscanf(reader, "%s %f %f", tempString, &tempX, &tempY);
			tempTextArray[textureCursor] = vec2(tempX, tempY);
			textureCursor++;
		}

		else if (reader[0] == 'v' & reader[1] == 'n')
		{

			sscanf(reader, "%s %f %f %f", tempString, &tempX, &tempY, &tempZ);
			tempNormArray[normalCursor] = vec4(tempX, tempY, tempZ, 0.0f);
			normalCursor++;
		}

		else if (reader[0] == 'f')
		{
			number_of_spaces = 0;
			for (int i = 0; i < strlen(reader); i++) {
				if (reader[i] == ' ') number_of_spaces++;
			}

			if (number_of_spaces == 3) {
				//std::cout << number_of_spaces << std::endl;

				sscanf(reader, "%s %s %s %s", tempString, tempString1, tempString2, tempString3);

				sscanf(tempString1, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString2, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString3, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;
			}
			else if (number_of_spaces = 4) {
				sscanf(reader, "%s %s %s %s %s", tempString, tempString1, tempString2, tempString3, tempString4);

				sscanf(tempString1, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString2, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString3, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString4, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString3, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;

				sscanf(tempString1, "%i %*c %i %*c %i", &intVertex, &intTexture, &intNormal);
				VertexOrder[triOrderCursor] = intVertex - 1;
				TextureOrder[triOrderCursor] = intTexture - 1;
				NormalOrder[triOrderCursor] = intNormal - 1;
				triOrderCursor++;
			}

		}
	} while (myfile.peek() != EOF);

	//create buffer material/////////////////////////////////
	vertexCursor = 0;
	std::cout << triangle_count_cue * 3 << std::endl;
	while (vertexCursor < triangle_count_player * 3) {
		PlayerVertBuf[vertexCursor] = tempVertArray[VertexOrder[vertexCursor]];
		PlayerTextBuf[vertexCursor] = tempTextArray[TextureOrder[vertexCursor]];
		PlayerNormBuf[vertexCursor] = tempNormArray[NormalOrder[vertexCursor]];
		//std::cout << BlenderNormBuf[vertexCursor] << std::endl;
		vertexCursor++;
	}
	std::cout << "FINISHED LOADING" << std::endl;
}


//Unloading all ressources here
void Scene::UnloadResource()
{
}

//Loading scene description here
void Scene::LoadScene(BallCollection* balls, Users* users)
{



	SceneBalls = balls;
	SceneUsers = users;




	//Let's use foot as the main units for measurement
	//Let's use coordinate in x-z plane (floor) with y=0 for the ground and y>0 for above ground
	//******** Light data here
	AmbientColor = vec4(0.2f, 0.2f, 0.2f, 0.0f)*1.5f;//white

	LightColor1 = vec4(0.93f, 0.89f, 0.69f, 0.0f)*1.7f;//soft white
	LightPosition1 = vec4(12.5f, 10.0f, 10.0f, 1.0f);
	LightHeading1 = vec4(12.5f, 3.0f, 10.0f, 1.0f) - LightPosition1;
	LightHeading1 = normalize(LightHeading1);//normalized it, last term is 0 (1-1 = 0) anyways

	LightColor2 = vec4(0.93f, 0.89f, 0.69f, 0.0f)*1.7f;//soft white
	LightPosition2 = vec4(7.5f, 10.0f, 10.0f, 1.0f);
	LightHeading2 = vec4(7.5f, 3.0f, 10.0f, 1.0f) - LightPosition2;
	LightHeading2 = normalize(LightHeading2);//normalized it, last term is 0 (1-1 = 0) anyways

	LightColor3 = vec4(0.93f, 0.89f, 0.69f, 0.0f)*1.7f;//soft white
	LightPosition3 = vec4(0.0f, 10.0f, 10.0f, 1.0f);
	LightHeading3 = vec4(-5.0f, 5.0f, 10.0f, 1.0f) - LightPosition3;
	LightHeading3 = normalize(LightHeading3);//normalized it, last term is 0 (1-1 = 0) anyways

											 //******** Load the grid here

											 //Let's draw a gridline every 5 feets on the floor in the [0,0]x[20,20] in x-z plane along the z axis with y=0;

	LightsCount = 3;
	vec4* vLightsVert = new vec4[LightsCount];
	int k = 0;
	//Add 2 last point to draw the light sources
	vLightsVert[k] = LightPosition1; k++;
	vLightsVert[k] = LightPosition2; k++;
	vLightsVert[k] = LightPosition3;

	//Create and bind a VAO for the grid
	glGenVertexArrays(1, &LightsObject);
	glBindVertexArray(LightsObject);

	//Grid vertices buffer handle 
	GLuint GridVertBuffer[1];
	//Ask for a new buffer number (generate)
	glGenBuffers(1, GridVertBuffer);
	///Make that buffer the current buffer (bind) 
	glBindBuffer(GL_ARRAY_BUFFER, GridVertBuffer[0]);
	//Send the data
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec4)*LightsCount, vLightsVert, GL_STATIC_DRAW);

	//Enable the vPosition & vColor attributes
	glEnableVertexAttribArray(GLT_ATTRIBUTE_VERTEX);
	//Indicating the vertex attributes organisation
	glVertexAttribPointer(GLT_ATTRIBUTE_VERTEX, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

	//Set the Grid color to green
	LightsColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);

	//Deallocate the data (it is on the gpu)
	delete[] vLightsVert;


	//***********ENVIRONMENT BUFFER LOADING HERE************//
	//Create and bind VAO
	glGenVertexArrays(1, &EnviObject);
	glBindVertexArray(EnviObject);

	GLuint EnviBuffer[3];
	glGenBuffers(3, EnviBuffer);

	glBindBuffer(GL_ARRAY_BUFFER, EnviBuffer[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec4)*triangle_count_envi * 3, EnviVertBuf, GL_STATIC_DRAW);
	glEnableVertexAttribArray(GLT_ATTRIBUTE_VERTEX);
	glVertexAttribPointer(GLT_ATTRIBUTE_VERTEX, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));



	glBindBuffer(GL_ARRAY_BUFFER, EnviBuffer[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec2)*triangle_count_envi * 3, EnviTextBuf, GL_STATIC_DRAW);
	glEnableVertexAttribArray(GLT_ATTRIBUTE_TEXTURE0);
	glVertexAttribPointer(GLT_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

	glBindBuffer(GL_ARRAY_BUFFER, EnviBuffer[2]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec4)*triangle_count_envi * 3, EnviNormBuf, GL_STATIC_DRAW);
	glEnableVertexAttribArray(GLT_ATTRIBUTE_NORMAL);
	glVertexAttribPointer(GLT_ATTRIBUTE_NORMAL, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

	EnviPosition = vec4(10.0f, 0.0f, 10.0f, 0.0f);

	//***********SURFACE BUFFER LOADING HERE************//
	glGenVertexArrays(1, &SurfaceObject);
	glBindVertexArray(SurfaceObject);

	GLuint SurfaceBuffer[3];
	glGenBuffers(3, SurfaceBuffer);

	glBindBuffer(GL_ARRAY_BUFFER, SurfaceBuffer[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec4)*triangle_count_surface * 3, SurfaceVertBuf, GL_STATIC_DRAW);
	glEnableVertexAttribArray(GLT_ATTRIBUTE_VERTEX);
	glVertexAttribPointer(GLT_ATTRIBUTE_VERTEX, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));



	glBindBuffer(GL_ARRAY_BUFFER, SurfaceBuffer[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec2)*triangle_count_surface * 3, SurfaceTextBuf, GL_STATIC_DRAW);
	glEnableVertexAttribArray(GLT_ATTRIBUTE_TEXTURE0);
	glVertexAttribPointer(GLT_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

	glBindBuffer(GL_ARRAY_BUFFER, SurfaceBuffer[2]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec4)*triangle_count_surface * 3, SurfaceNormBuf, GL_STATIC_DRAW);
	glEnableVertexAttribArray(GLT_ATTRIBUTE_NORMAL);
	glVertexAttribPointer(GLT_ATTRIBUTE_NORMAL, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));


	//***********BALL BUFFER LOADING HERE************//
	//Create and bind VAO
	glGenVertexArrays(1, &BallObject);
	glBindVertexArray(BallObject);

	GLuint BallBuffer[3];
	glGenBuffers(3, BallBuffer);

	glBindBuffer(GL_ARRAY_BUFFER, BallBuffer[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec4)*triangle_count_ball * 3, BallVertBuf, GL_STATIC_DRAW);
	glEnableVertexAttribArray(GLT_ATTRIBUTE_VERTEX);
	glVertexAttribPointer(GLT_ATTRIBUTE_VERTEX, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

	glBindBuffer(GL_ARRAY_BUFFER, BallBuffer[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec2)*triangle_count_ball * 3, BallTextBuf, GL_STATIC_DRAW);
	glEnableVertexAttribArray(GLT_ATTRIBUTE_TEXTURE0);
	glVertexAttribPointer(GLT_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

	glBindBuffer(GL_ARRAY_BUFFER, BallBuffer[2]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec4)*triangle_count_ball * 3, BallNormBuf, GL_STATIC_DRAW);
	glEnableVertexAttribArray(GLT_ATTRIBUTE_NORMAL);
	glVertexAttribPointer(GLT_ATTRIBUTE_NORMAL, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

	//***********CUE BUFFER LOADING HERE************//
	//Create and bind VAO
	glGenVertexArrays(1, &CueObject);
	glBindVertexArray(CueObject);

	GLuint CueBuffer[3];
	glGenBuffers(3, CueBuffer);

	glBindBuffer(GL_ARRAY_BUFFER, CueBuffer[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec4)*triangle_count_cue * 3, CueVertBuf, GL_STATIC_DRAW);
	glEnableVertexAttribArray(GLT_ATTRIBUTE_VERTEX);
	glVertexAttribPointer(GLT_ATTRIBUTE_VERTEX, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

	glBindBuffer(GL_ARRAY_BUFFER, CueBuffer[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec2)*triangle_count_cue * 3, CueTextBuf, GL_STATIC_DRAW);
	glEnableVertexAttribArray(GLT_ATTRIBUTE_TEXTURE0);
	glVertexAttribPointer(GLT_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

	glBindBuffer(GL_ARRAY_BUFFER, CueBuffer[2]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec4)*triangle_count_cue * 3, CueNormBuf, GL_STATIC_DRAW);
	glEnableVertexAttribArray(GLT_ATTRIBUTE_NORMAL);
	glVertexAttribPointer(GLT_ATTRIBUTE_NORMAL, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

	//***********PLAYER BUFFER LOADING HERE************//
	//Create and bind VAO
	glGenVertexArrays(1, &PlayerObject);
	glBindVertexArray(PlayerObject);

	GLuint PlayerBuffer[3];
	glGenBuffers(3, PlayerBuffer);

	glBindBuffer(GL_ARRAY_BUFFER, PlayerBuffer[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec4)*triangle_count_player * 3, PlayerVertBuf, GL_STATIC_DRAW);
	glEnableVertexAttribArray(GLT_ATTRIBUTE_VERTEX);
	glVertexAttribPointer(GLT_ATTRIBUTE_VERTEX, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

	glBindBuffer(GL_ARRAY_BUFFER, PlayerBuffer[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec2)*triangle_count_player * 3, PlayerTextBuf, GL_STATIC_DRAW);
	glEnableVertexAttribArray(GLT_ATTRIBUTE_TEXTURE0);
	glVertexAttribPointer(GLT_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

	glBindBuffer(GL_ARRAY_BUFFER, PlayerBuffer[2]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec4)*triangle_count_player * 3, PlayerNormBuf, GL_STATIC_DRAW);
	glEnableVertexAttribArray(GLT_ATTRIBUTE_NORMAL);
	glVertexAttribPointer(GLT_ATTRIBUTE_NORMAL, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
}


//Unloading scene description here
void Scene::UnloadScene()
{
}



void Scene::OnDraw()
{
	vec4 camera_pos = SceneUsers->getCameraPos();
	float camera_angle_yaw = SceneUsers->getCameraAngleYaw();
	float camera_angle_pitch = SceneUsers->getCameraAnglePitch();
	//move world with respect to camera
	mat4 viewTrMat = Translate(-camera_pos.x, -camera_pos.y, -camera_pos.z);
	//rotate world with respect to camera
	mat4 viewRotMatYaw = RotateY(-camera_angle_yaw);
	mat4 viewRotMatPitch = RotateX(-camera_angle_pitch);
	//create the projection view matrix
	mat4 projMat = Frustum(-0.1, +0.1,
		-0.1, +0.1,
		0.1, 40);


	//******** Draw the lights**************************************************************************

	//Select Program and Grid object
	glUseProgram(ColorShaderID);
	glBindVertexArray(LightsObject);

	glUniform4f(ColorUnifLoc, AmbientColor.x, AmbientColor.y, AmbientColor.z, AmbientColor.w);

	//Set the uniform for the viewMat matrix
	GLint locViewMat = glGetUniformLocation(ColorShaderID, "viewMat");
	glUniformMatrix4fv(locViewMat, 1, GL_TRUE, projMat*viewRotMatPitch*viewRotMatYaw*viewTrMat);

	//Draw
	glEnable(GL_DEPTH_TEST);
	glLineWidth(3);

	//*****Draw the light sources
	//Create the uniforms for color

	glUniform4f(ColorUnifLoc, LightColor1.x, LightColor1.y, LightColor1.z, LightColor1.w);
	glPointSize(20);
	glDrawArrays(GL_POINTS, 0, 1);
	glDrawArrays(GL_POINTS, 1, 1);
	glDrawArrays(GL_POINTS, 2, 1);

	//****************draw environnment*****************************************//

	//Select Program and Scene object
	glUseProgram(LightShaderID);
	glBindVertexArray(EnviObject);

	glUniform1i(TexUnitLoc, 0);
	//Create the uniforms for ambiant color
	glUniform4f(AmbientColorLoc, AmbientColor.x, AmbientColor.y, AmbientColor.z, AmbientColor.w);

	//Create the uniforms light position and color
	glUniform4f(LightPosition1Loc, LightPosition1.x, LightPosition1.y, LightPosition1.z, LightPosition1.w);
	glUniform4f(LightColor1Loc, LightColor1.x, LightColor1.y, LightColor1.z, LightColor1.w);
	glUniform4f(LightHeading1Loc, LightHeading1.x, LightHeading1.y, LightHeading1.z, LightHeading1.w);

	glUniform4f(LightPosition2Loc, LightPosition2.x, LightPosition2.y, LightPosition2.z, LightPosition2.w);
	glUniform4f(LightColor2Loc, LightColor2.x, LightColor2.y, LightColor2.z, LightColor2.w);
	glUniform4f(LightHeading2Loc, LightHeading2.x, LightHeading2.y, LightHeading2.z, LightHeading2.w);

	glUniform4f(LightPosition3Loc, LightPosition3.x, LightPosition3.y, LightPosition3.z, LightPosition3.w);
	glUniform4f(LightColor3Loc, LightColor3.x, LightColor3.y, LightColor3.z, LightColor3.w);
	glUniform4f(LightHeading3Loc, LightHeading3.x, LightHeading3.y, LightHeading3.z, LightHeading3.w);

	glUniform4f(CameraPositionLoc, camera_pos.x, camera_pos.y, camera_pos.z, camera_pos.w);


	for (int i = 0; i < 16; i++)
	{
		ballPosArray[i] = SceneBalls->getBallPosition(i);
	}


	glUniform4fv(glGetUniformLocation(LightShaderID, "vBallPos"), 16, (float*)ballPosArray);
	calcShadowLoc = 0;
	glUniform1i(calcShadow, calcShadowLoc);
	specularReflectionLoc = 0;
	glUniform1i(specularReflection, specularReflectionLoc);

	glBindTexture(GL_TEXTURE_2D, EnviTexID);


	//Move 
	mat4 objTrMat = Translate(EnviPosition.x, EnviPosition.y, EnviPosition.z);
	mat4 objRotMat = RotateY(0);

	//Send it
	glUniformMatrix4fv(ViewMatrixLoc, 1, GL_TRUE, projMat*viewRotMatPitch*viewRotMatYaw*viewTrMat*objTrMat*objRotMat); //Done to everything
	glUniformMatrix4fv(ObjMatrixLoc, 1, GL_TRUE, objTrMat*objRotMat); //Done to move object in world coordinates only
	glUniformMatrix4fv(NormMatrixLoc, 1, GL_TRUE, objRotMat); //Done for object surface normla only (object to wold rotations)

															  //Draw
	glEnable(GL_DEPTH_TEST);

	glDrawArrays(GL_TRIANGLES, 0, triangle_count_envi * 3);

	//*******************DRAW SURFACE************//

	//Select Program and Scene object
	glUseProgram(LightShaderID);
	glBindVertexArray(SurfaceObject);


	glUniform4fv(glGetUniformLocation(LightShaderID, "vBallPos"), 16, (float*)ballPosArray);
	calcShadowLoc = 1;
	glUniform1i(calcShadow, calcShadowLoc);

	glBindTexture(GL_TEXTURE_2D, SurfaceTexID);


	//Move 
	objTrMat = Translate(EnviPosition.x, EnviPosition.y, EnviPosition.z);
	objRotMat = RotateY(0);

	//Send it
	glUniformMatrix4fv(ViewMatrixLoc, 1, GL_TRUE, projMat*viewRotMatPitch*viewRotMatYaw*viewTrMat*objTrMat*objRotMat); //Done to everything
	glUniformMatrix4fv(ObjMatrixLoc, 1, GL_TRUE, objTrMat*objRotMat); //Done to move object in world coordinates only
	glUniformMatrix4fv(NormMatrixLoc, 1, GL_TRUE, objRotMat); //Done for object surface normla only (object to wold rotations)

															  //Draw
	glEnable(GL_DEPTH_TEST);

	glDrawArrays(GL_TRIANGLES, 0, triangle_count_surface * 3);

	calcShadowLoc = 0;
	glUniform1i(calcShadow, calcShadowLoc);




	//****************************************************************************************************************************************************************
	//****************************************************************************************************************************************************************
	//Loop for the 16 balls? The only thing that changes is the position and the texture used
	//****************************************************************************************************************************************************************
	//****************************************************************************************************************************************************************
	glBindVertexArray(BallObject);

	specularReflectionLoc = 1;
	glUniform1i(specularReflection, specularReflectionLoc);

	for (int i = 0; i < 16; i++)
	{
		BallRotation = SceneBalls->getBallRotation(i);
		objRotMat = RotateX(BallRotation.x)*RotateY(BallRotation.y)*RotateZ(BallRotation.z);
		BallPosition = SceneBalls->getBallPosition(i);
		objTrMat = Translate(BallPosition.x, BallPosition.y, BallPosition.z);
		glUniformMatrix4fv(ViewMatrixLoc, 1, GL_TRUE, projMat*viewRotMatPitch*viewRotMatYaw*viewTrMat*objTrMat*objRotMat); //Done to everything
		glUniformMatrix4fv(ObjMatrixLoc, 1, GL_TRUE, objTrMat*objRotMat); //Done to move object in world coordinates only
		glUniformMatrix4fv(NormMatrixLoc, 1, GL_TRUE, objRotMat); //Done for object surface normla only (object to wold rotations)
		glBindTexture(GL_TEXTURE_2D, BallTexture[i]);
		glDrawArrays(GL_TRIANGLES, 0, triangle_count_ball * 3);
	}


	///////DRAW PLAYER////////////////////

	SceneUsers->switchU();
	PlayerPosition = (SceneUsers->getCameraPos());
	objRotMat = RotateY(SceneUsers->getCameraAngleYaw()+90);
	objTrMat = Translate(PlayerPosition.x, PlayerPosition.y - 5.0f, PlayerPosition.z);
	SceneUsers->switchU();

	specularReflectionLoc = 0;
	glUniform1i(specularReflection, specularReflectionLoc);

	glBindVertexArray(PlayerObject);
	glUniformMatrix4fv(ViewMatrixLoc, 1, GL_TRUE, projMat*viewRotMatPitch*viewRotMatYaw*viewTrMat*objTrMat*objRotMat); //Done to everything
	glUniformMatrix4fv(ObjMatrixLoc, 1, GL_TRUE, objTrMat*objRotMat); //Done to move object in world coordinates only
	glUniformMatrix4fv(NormMatrixLoc, 1, GL_TRUE, objRotMat);


	glBindTexture(GL_TEXTURE_2D, PlayerTexID[SceneUsers->getCurrentUser()]);
	glDrawArrays(GL_TRIANGLES, 0, triangle_count_player * 3);

	///////DRAW CUE////////////////////

	float cueOffSetY = -1.5f;
	CuePosition = (SceneUsers->getCameraPos());
	if (CuePosition.y < 5.0f) {
		cueOffSetY = -0.5f;
	}
	mat4 cueOffset = Translate(0.0f, 0.0f, cueOffSetY);
	objTrMat = Translate(CuePosition.x, CuePosition.y, CuePosition.z);
	objRotMat = RotateY(0);
	float deltaX = SceneBalls->getBallPosition(0).x - SceneUsers->getCameraPos().x;
	float deltaY = SceneBalls->getBallPosition(0).y - SceneUsers->getCameraPos().y - cueOffSetY;
	float deltaZ = SceneBalls->getBallPosition(0).z - SceneUsers->getCameraPos().z;
	

	float distance_player_ballXZ = sqrt(deltaX*deltaX + deltaZ * deltaZ);


	mat4 cueTraFromWhite = Translate(0, -1, 0);
	if (distance_player_ballXZ < 4.0) {
		cueTraFromWhite = Translate(0, -(4.0 - distance_player_ballXZ)-1,0);
	}

	float cue_angle_yaw = 180+acos(deltaX/ distance_player_ballXZ)*180/M_PI;
	if (deltaZ > 0) {
		cue_angle_yaw *= -1;
	}
	float cue_angle_pitch = acos((deltaX*deltaX+deltaZ*deltaZ) / (sqrt(deltaX*deltaX + deltaZ * deltaZ)*sqrt(deltaX*deltaX +deltaY*deltaY+ deltaZ * deltaZ))) * 180 / M_PI;
	//cue_angle_pitch = 0.0f;

	viewRotMatYaw = RotateX(270.0f - cue_angle_pitch)*RotateZ(90+cue_angle_yaw- camera_angle_yaw);
	mat4 cueRotMat = RotateY(camera_angle_yaw)*RotateX(270.0f - cue_angle_pitch);

	specularReflectionLoc = 1;
	glUniform1i(specularReflection, specularReflectionLoc);

	glBindVertexArray(CueObject);
	glUniformMatrix4fv(ViewMatrixLoc, 1, GL_TRUE, projMat*viewRotMatPitch*viewRotMatYaw*viewTrMat*objTrMat*objRotMat*cueOffset*cueTraFromWhite); //Done to everything
	glUniformMatrix4fv(ObjMatrixLoc, 1, GL_TRUE, objTrMat*cueRotMat*objRotMat*cueOffset*cueTraFromWhite); //Done to move object in world coordinates only -- amener la camera (pole) a 0,0, tourner, puis re translater
	glUniformMatrix4fv(NormMatrixLoc, 1, GL_TRUE, cueRotMat*objRotMat);
	glBindTexture(GL_TEXTURE_2D, CueTexID);
	glDrawArrays(GL_TRIANGLES, 0, triangle_count_cue * 3);
}

void Scene::ResetUserPosition()///////////////////////////////////////////////////////////
{

}