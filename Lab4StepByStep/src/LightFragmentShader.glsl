#version 330 core

//All positions are in world coordinates
uniform sampler2D texID;		//Texture unit number
uniform vec4 vAmbientColor;		//Ambient light
uniform vec4 vLightColor1;		//Light 1 color
uniform vec4 vLightPosition1;	//Light 1 position
uniform vec4 vLightHeading1;	//Light 1 heading (for spotlight)
uniform vec4 vLightColor2;		//Light 2 color
uniform vec4 vLightPosition2;	//Light 2 position
uniform vec4 vLightHeading2;	//Light 2 heading (for spotlight)
uniform vec4 vLightColor3;		//Light 3 color
uniform vec4 vLightPosition3;	//Light 3 position
uniform vec4 vLightHeading3;	//Light 3 heading (for spotlight)

uniform vec4 vCameraPosition;	//Camera position (for specular light)

uniform vec4 vBallPos[16];			//Balls positions
uniform int vcalcShadow;		//True if drawiing table
uniform int cshowReflections;

smooth in vec4 vWorldCoords;	//Fragment world coordinates
smooth in vec2 vVarTexCoords0;	//Fragment texture coordinates
flat in vec3 vFlatNormals;		//Fragment surface normals (flat)

out vec4 gl_FragColor;			//Fragment output color


float norm(vec3 segment)
{
	float l = sqrt(segment.x*segment.x + segment.y*segment.y + segment.z*segment.z);
	return l;
}

vec3 CrossProduct (vec3 A, vec3 B){
	float x = A.y * B.z - A.z * B.y;
	float y = A.x * B.z - A.z * B.x;
	float z = A.x * B.y - A.y * B.x;
	return (vec3 (x, y, z));
}
float distancePointLine (vec3 point, vec3 linepoint1, vec3 linepoint2){
	float num = norm(CrossProduct(linepoint2-linepoint1,linepoint1-point));
	float den = norm(linepoint2-linepoint1);
	return num/den;
}

void main()
{

	//Compute the ambient color
	vec4 ambient = texture(texID, vVarTexCoords0.st)*vAmbientColor;

	float shadow1 = 1.0f;
	float shadow2 = 1.0f;

	if (vcalcShadow==1){
	for (int i = 0; i<16; i++){
		float d = distancePointLine(vBallPos[i].xyz,vWorldCoords.xyz,vLightPosition1.xyz);
		if (d<0.1875f/2){
			float ratio = d/(0.1875f/2);
			shadow1 = shadow1*ratio*ratio*ratio;      //vec4(0.1f, 0.1f, 0.1f, 0.0f);
		}
	}
	for (int i = 0; i<16; i++){
		float d = distancePointLine(vBallPos[i].xyz,vWorldCoords.xyz,vLightPosition2.xyz);
		if (d<0.1875f/2){
			float ratio = d/(0.1875f/2);
			shadow2 = shadow2*ratio*ratio*ratio;      //vec4(0.1f, 0.1f, 0.1f, 0.0f);
		}
	}
	}


	//Compute the diffuse color from light 1 (no distance effect, no spotlight effect)
	vec3 vToLight1 = normalize(vLightPosition1.xyz-vWorldCoords.xyz);
	vec4 diffuse1 = texture(texID, vVarTexCoords0.st)*vLightColor1*max(0,dot(vFlatNormals,vToLight1));
	diffuse1 = diffuse1*max(0,pow(dot(vLightHeading1.xyz,-vToLight1),20));

	//Compute the diffuse color from light 2 (no distance effect, no spotlight effect)
	vec3 vToLight2 = normalize(vLightPosition2.xyz-vWorldCoords.xyz);
	vec4 diffuse2 = texture(texID, vVarTexCoords0.st)*vLightColor2*max(0,dot(vFlatNormals,vToLight2));
	diffuse2 = diffuse2*max(0,pow(dot(vLightHeading2.xyz,-vToLight2),20));

	//Compute the diffuse color from light 3 (no distance effect, no spotlight effect)
	vec3 vToLight3 = normalize(vLightPosition3.xyz-vWorldCoords.xyz);
	vec4 diffuse3 = texture(texID, vVarTexCoords0.st)*vLightColor3*max(0,dot(vFlatNormals,vToLight3));
	diffuse3 = diffuse3*max(0,pow(dot(vLightHeading3.xyz,-vToLight3),20));

	//Add light 1 specularity, assuming material is uncolored for specular light (no distance effect, no spotlight effect)
	vec3 vReflection = reflect(-vToLight1,vFlatNormals);
	vec3 vToCamera = normalize(vCameraPosition.xyz-vWorldCoords.xyz);
	vec4 specular1 = vLightColor1*max(0,pow(dot(vReflection,vToCamera),120));

	//Add light 2 specularity, assuming material is uncolored for specular light (no distance effect, no spotlight effect)
	vReflection = reflect(-vToLight2,vFlatNormals);
	vec4 specular2 = vLightColor2*max(0,pow(dot(vReflection,vToCamera),120));

	vec4 totalspecular = vec4(0.0f,0.0f,0.0f,0.0f);
	if (cshowReflections==1){
		totalspecular=totalspecular+specular1+specular2;
	}
	


	//Generate final fragment color
    gl_FragColor = ambient + diffuse1*shadow1 + diffuse2*shadow2 + diffuse3+ totalspecular;

}
