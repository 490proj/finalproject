
#include "SoundEngine.h"

SoundEngine::SoundEngine(BallCollection* B, Users* U)
{
	m_U = U;
	m_B = B;

}

SoundEngine::~SoundEngine()
{
	engine->stopAllSounds();
	engine->drop();
}

bool SoundEngine::SoundLoad() {
	//Initiate the sound engine. Return an error if not loaded correctly
	engine = irrklang::createIrrKlangDevice();
	//Check if successful
	if (!engine) { // error starting up the engine
		std::cout << "Could not start engine!" << std::endl;
		return false;
	}
	else { std::cout << "Engine initiated successfully!" << std::endl; }

	//Set the engine volume (master volume)
	engine->setSoundVolume(SE_MVol);

	//Load the songs and place them in the array
	songsList[0] = engine->addSoundSourceFromFile("../Assets/KP-F.ogg")->getName();
	songsList[1] = engine->addSoundSourceFromFile("../Assets/BR-SS.ogg")->getName();
	songsList[2] = engine->addSoundSourceFromFile("../Assets/AAL-PhysEd.ogg")->getName();
	songsList[3] = engine->addSoundSourceFromFile("../Assets/F-JH.ogg")->getName();
	songsList[4] = engine->addSoundSourceFromFile("../Assets/G-SB.ogg")->getName();
	songsList[5] = engine->addSoundSourceFromFile("../Assets/ID-R90.ogg")->getName();
	songsList[6] = engine->addSoundSourceFromFile("../Assets/PSY-GS.ogg")->getName();

	//Load the sounds
	ball_Ball = engine->addSoundSourceFromFile("../Assets/s_ballBounce.ogg");
	ball_Wall = engine->addSoundSourceFromFile("../Assets/s_wallBounce.ogg");
	ball_Pole = engine->addSoundSourceFromFile("../Assets/s_poleHit.ogg");
	ball_Hole = engine->addSoundSourceFromFile("../Assets/Intervention.ogg");
	ball_R2M = engine->addSoundSourceFromFile("../Assets/YAY.ogg");
	ball_Roll = engine->addSoundSourceFromFile("../Assets/s_ballRoll.ogg");
	ball_Roll_Snd = engine->play2D(ball_Roll, false, true, true, false);

	//Step variation sounds list
	stepSounds[0] = engine->addSoundSourceFromFile("../Assets/s_stepVar1.ogg")->getName();
	stepSounds[1] = engine->addSoundSourceFromFile("../Assets/s_stepVar2.ogg")->getName();
	stepSounds[2] = engine->addSoundSourceFromFile("../Assets/s_stepVar3.ogg")->getName();
	stepSounds[3] = engine->addSoundSourceFromFile("../Assets/s_stepVar4.ogg")->getName();

	player_ScoreG = engine->addSoundSourceFromFile("../Assets/WinHorn.ogg");
	player_ScoreB = engine->addSoundSourceFromFile("../Assets/LoseHorn.ogg");
	player_Win = engine->addSoundSourceFromFile("../Assets/OMS.ogg");
	player_Lose = engine->addSoundSourceFromFile("../Assets/DAWAE.ogg");

	return true;
}

void SoundEngine::keyControls(unsigned char key) {
	switch (key) {
	case 'y': SE_JukeVol += 0.05;	//Volume up 
		if (SE_JukeVol > 1.00f) {
			SE_JukeVol = 1.00f;
		}
		current_track->setVolume(SE_JukeVol);
		break;
	case 'h': SE_JukeVol -= 0.05;	//Volume down
		if (SE_JukeVol < 0.00f) {
			SE_JukeVol = 0.00f;
		}
		current_track->setVolume(SE_JukeVol);
		break;
	case 'k':		//Stop track
		if (!current_track->isFinished()) {
			current_track->stop();
		}
		else {
			current_track = engine->play2D(songsList[SE_index_songsList], false, true, true, ESM_AUTO_DETECT, true);
			current_track->setPlayPosition(0);
			current_track->setIsPaused(false);
		}
		break;
	case 'i':		//Pause/play track
		if (!current_track->getIsPaused()) {		//If it isn't paused: get the playback time, pause the sound and set the playback time to what was pulled
			playPos = current_track->getPlayPosition();
			current_track->setIsPaused(true);
			current_track->setPlayPosition(playPos);
		}
		else {	//If the track is not playing: play the sound
			current_track->setIsPaused(false);
		}
		break;
	case 'j':		//Play previous track
		if (current_track->getPlayPosition() > 5000) {	//If track has been playing for more than 5 seconds, return to start
			current_track->setIsPaused(true);
			current_track->setPlayPosition(0);
			current_track->setIsPaused(false);
		}
		else {		//Otherwise, set the current track to the previous one
			SE_index_songsList -= 2;
			if (SE_index_songsList < 0) {
				SE_index_songsList += SE_SongsCount;
			}
			current_track->stop();
			current_track = engine->play2D(songsList[SE_index_songsList], true, false, true);
			SE_index_songsList = (SE_index_songsList + 1) % SE_SongsCount;
		}
		break;
	case 'l':		//Play next track
		current_track->stop();
		current_track = engine->play2D(songsList[SE_index_songsList], true, false, true);
		SE_index_songsList = (SE_index_songsList + 1) % SE_SongsCount;
		break;
	case 8:		//[backspace] KILL COMMAND
		engine->stopAllSounds();
		break;

	}

	//Update the engine to make sure it commits the changes
	engine->update();
	return;
}


void SoundEngine::update(vec4 camera_pos, vec3 camera_angle) {

	//***** Mettre � jour l'information du joueur
	vec3df position = vec3_df(camera_pos); // Position of the listener
	vec3df lookDirection = vec3_df(camera_angle);	// The direction the listener looks into
	vec3df velPerSecond(0, 0, 0);    // Only relevant for doppler effects
	vec3df upVector(0, 1, 0);        // Where 'up' is in your 3D scene

	//*****Appliquer les modifications au moteur
	engine->setListenerPosition(position, lookDirection, velPerSecond, upVector);
	engine->update();
}

void SoundEngine::SoundEvents(vec3 Position, int se_sound_event) {//ball collection is m_B
	if (se_sound_event != 0) {
		sndLoc = vec3_df(Position);
		switch (se_sound_event) {

			//***** BALL EVENTS
		case 1: //When 2 balls come in contact with each other
			
			ball_Ball_col1 = engine->play3D(ball_Ball, sndLoc, false, false, true, false);
			ball_Ball_col1->setMinDistance(minDist);

			//Update the play position of the balls' rolling sound (proportional to Physics->strength)
			ball_Roll_Snd->setPlayPosition(rollSoundLength * getNorm(m_B->getBallVelocity(0)) / 0.040f);
			ball_Roll_Snd->setIsPaused(true);
			break;

		case 2:	//When a ball hits a wall

			ball_Wall_col1 = engine->play3D(ball_Wall, sndLoc, false, false, true, false);
			ball_Wall_col1->setMinDistance(minDist);
			//Update the play position of the balls' rolling sound (proportional to Physics->strength)
			ball_Roll_Snd->setPlayPosition(rollSoundLength * getNorm(m_B->getBallVelocity(0)) / 0.040f);
			ball_Roll_Snd->setIsPaused(true);
			break;

		case 3:	//When the pole strikes the ball

			engine->play3D(ball_Pole, sndLoc, false, false, true, false)->setMinDistance(minDist);

			break;

		case 4:	//When the ball enters a pouch

			engine->play3D(ball_Hole, sndLoc, false, false, true, false)->setMinDistance(minDist);
			engine->play2D(ball_R2M, false, false, true, false);

			break;

		case 5:	//When the ball rolls

			ball_Roll_Snd = engine->play3D(ball_Roll, sndLoc, false, true, true, false);
			ball_Roll_Snd->setMinDistance(minDist);
			ball_Roll_Snd->setPlayPosition(rollSoundLength * getNorm(m_B->getBallVelocity(0))/0.040f); 
			ball_Roll_Snd->setIsPaused(true);

			break;

		case 6:	//When the ball rotates (curve ball)
			//std::cout << "Insert spinning sound here" << std::endl;
			break;

			//***** PLAYER EVENTS
		case 7:	//When the player moves, set the step sound from stepSounds[4]
			sndLoc.Y = 0.0f;
			//DEBUG - joues le son en 2D pour confirmer que le son marche (si on ne l'entends pas, c'est que le joueur est trop loin)
			//player_Step = engine->play2D(stepSounds[SE_index_stepVars], false, false, true, ESM_AUTO_DETECT, false);
			player_Step = engine->play3D(stepSounds[SE_index_stepVars], sndLoc, false, false, true, ESM_AUTO_DETECT, false);
			player_Step->setMinDistance(minDist);
			SE_index_stepVars = (SE_index_stepVars + 1) % SE_stepVars;
			
			break;

		case 8:	//DUCK
			engine->play2D("../Assets/DUCK.ogg", false, false, true, ESM_AUTO_DETECT, false); 
			break; 
		}
	

		// Il n'y a pas de mani�re de savoir si la balle appartient au joueur qui l'a empoch�e. Le code est comment� en attendant
		/*
		if (false) {	//When the player scores
			if (sunk his ball) {
				engine->play3D(player_ScoreG, sndLoc);
			}
			else {
				engine->play3D(player_ScoreB, sndLoc);
			}

			//At the end of the game
			if (player won) {
				engine->play3D(player_Win, sndLoc, false, false, true, false);
			}
			else {
				engine->play3D(player_Lose, sndLoc, false, false, true, false);
			}
		}
		*/

	}
	else {
		if (SE_startUp) {
			//Use this block if starting/restarting the jukebox
			srand(time(NULL));
			SE_index_songsList = rand() % SE_SongsCount;
			current_track = engine->play2D(songsList[SE_index_songsList], false, true, true, irrklang::ESM_AUTO_DETECT, false);
			SE_index_songsList = (SE_index_songsList + 1) % SE_SongsCount;
			current_track->setVolume(SE_JukeVol);
			//current_track->setIsPaused(false);	// Le jukebox d�marre � pause si cette ligne est comment�e
			SE_startUp = false;
		}
		else {
			if (current_track->isFinished()) {
				current_track = engine->play2D(songsList[SE_index_songsList], false, true, true, irrklang::ESM_AUTO_DETECT, true);
				current_track->setVolume(SE_JukeVol);
				current_track->setIsPaused(false);
				SE_index_songsList = (SE_index_songsList + 1) % SE_SongsCount;
			}
		}
	}
}


