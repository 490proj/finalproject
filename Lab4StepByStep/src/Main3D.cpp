//Include the stardard C++ IO stream
#include <iostream>
#include <math.h>


//Include GLEW and GLUT
#define GLEW_STATIC
#include <GL/glew.h>		
#include <GL/glut.h>
#include <irrklang/irrKlang.h>
#pragma comment(lib, "irrKlang.lib") // link with irrKlang.dll

//Include ICG headers
#include <Angel.h>

//Include Scene header
#include "Scene.h"


#include "Balls.h"
#include "Physics.h"
#include "SoundEngine.h"
#include "UserDetail.h"

BallCollection* B;
SoundEngine* S;
Physics* P;
Users* U;


//Create global variables
Scene myScene;			//Scene object
bool enable_mouse_movement = true;
vec4 CueBallPosition;
vec4 BallPosition;
vec4 BallVelocity;


/****************************************************************************\
* Callback function for timer-based rotation
* Arguments:
*     val valuer pass to function
\****************************************************************************/
void Timer(int val) {
	//std::cout << "Callback!" << std::endl;
	P->update();
	glutPostRedisplay();
}

/****************************************************************************\
 * Callback function when the window needs to be redrawn  
\****************************************************************************/
void RenderScene() {
	//Generate a black background
	glClearColor(0.0f,0.0f, 0.0f, 0.0f );
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	//Call scene OnDraw
	myScene.OnDraw();

	//glFlush();
	//Swap buffers 
	glutSwapBuffers();

	//Start next timer
	glutTimerFunc(50, Timer, 1);
}


/****************************************************************************\
 * Callback function when hitting a special key
 * Arguments:
 *     key is the hitted key
 *     x  is the mouse x coordinate
 *     y  is the mouse y coordinate
\****************************************************************************/
void Controller(int key, int x, int y)
{
	glutPostRedisplay();

	//S->update(U->getCameraPos(), vec3(sin((U->getCameraAngleYaw() + 180)*M_PI / 180), U->getCameraAnglePitch, cos((U->getCameraAngleYaw() + 180)*M_PI / 180)));
}

/****************************************************************************\
* Callback function mouse is mouved
* Arguments:
*     x  is the mouse x coordinate
*     y  is the mouse y coordinate
\****************************************************************************/
void MouseController(int x, int y)
{
	if (enable_mouse_movement)
	{
		float camera_angle_yaw = U->getCameraAngleYaw();
		float camera_angle_pitch = U->getCameraAnglePitch();
		float deltaX = (x - 1200.0f / 2.0f) / 5.0f;
		float deltaY = (y - 900.0f / 2.0f) / 5.0f;

		if (camera_angle_pitch - deltaY>-90 & camera_angle_pitch - deltaY<90) {
			U->setCameraAnglePitch(camera_angle_pitch-deltaY);
		}

		U->setCameraAngleYaw(camera_angle_yaw-deltaX);
		
		enable_mouse_movement = false;
		glutWarpPointer(1200.0f / 2.0f, 900.0f / 2.0f);
		glutPostRedisplay();
	}
	else
	{
		enable_mouse_movement = true;
	}
}
/****************************************************************************\
 * Callback function when hitting a special key
 * Arguments:
 *     key   ASCII caracter hitted
 *     x  is the mouse x coordinate
 *     y  is the mouse y coordinate
\****************************************************************************/
void Controller2(unsigned char key, int x, int y)
{
	
	vec4 new_pos = U->getCameraPos();
	vec4 camera_pos = U->getCameraPos();
	float camera_angle_yaw = U->getCameraAngleYaw();
	float movement_speed = U->getMovSpeed();
	switch (key)
	{
		//Camera motion
	case 'w': //Forward
		new_pos = camera_pos + movement_speed * vec4(sin((camera_angle_yaw + 180)*M_PI / 180), 0.0f, cos((camera_angle_yaw + 180)*M_PI / 180), 0.0f);
		S->SoundEvents(vec3(new_pos.x, new_pos.y, new_pos.z), 7);
		break;
	case 's': //Backward
		new_pos = camera_pos - movement_speed*vec4(sin((camera_angle_yaw + 180)*M_PI / 180), 0.0f, cos((camera_angle_yaw + 180)*M_PI / 180), 0.0f);
		S->SoundEvents(vec3(new_pos.x, new_pos.y, new_pos.z), 7);
		break;
	case 'a': //Leftward
		new_pos = camera_pos + movement_speed*vec4(sin(((camera_angle_yaw + 180) + 90)*M_PI / 180), 0.0f, cos(((camera_angle_yaw + 180) + 90)*M_PI / 180), 0.0f);
		S->SoundEvents(vec3(new_pos.x, new_pos.y, new_pos.z), 7);
		break;
	case 'd': //Rightward
		new_pos = camera_pos - movement_speed*vec4(sin(((camera_angle_yaw + 180) + 90)*M_PI / 180), 0.0f, cos(((camera_angle_yaw + 180) + 90)*M_PI / 180), 0.0f);
		S->SoundEvents(vec3(new_pos.x, new_pos.y, new_pos.z), 7);
		break;
		//Object motion
	case 'c':
		new_pos = U->toggleCrouch();
		break;
	case 'x':
		exit(0);
		break;
	case 'z':
		U->switchCurrentUser();
		new_pos = U->getCameraPos();
		break;
	case '1':
		P->SetStrength('1');
		break;
	case '2':
		P->SetStrength('2');
		break;
	case '3':
		P->SetStrength('3');
		break;
	case '4':
		P->SetStrength('4');
		break;
	case '5':
		P->SetStrength('5');
		break;
	case 'r':
		B->setBallPosition(0, { 10.0f - 2.36f, 2.75f + 0.1875f/2, 10.0f, 0.0f });
		B->setBallVelocity(0, { 0.0f, 0.0f, 0.0f, 0.0f });
		B->trueOnTable(0);
		break;
	case 'R':
		B->Reset();
		break;
	case ' ':
		BallPosition = B->getBallPosition(0);
		BallVelocity = B->getBallVelocity(0);
		if (!P->MovementCheck()) {
			BallVelocity = P->BallInitialMovement(camera_pos, BallPosition);
			B->setBallVelocity(0, BallVelocity);
			S->SoundEvents(vec3(BallPosition.x, BallPosition.y, BallPosition.z), 3);
			//S->SoundEvents(vec3(BallPosition.x, BallPosition.y, BallPosition.z), 3);
		}
		else { std::cout << "Some of the balls are still moving" << std::endl; };
		break;

	case 'k':	//[kilo] Pause/play track
		S->keyControls(key);
		break;
	case 'i':	//[india] Stop current track
		S->keyControls(key);
		break;
	case 'j':	//[juliet] Previous track
		S->keyControls(key);
		break;
	case 'l':	//[lima] Next track
		S->keyControls(key);
		break;
	case 'y':	//[yankee] Volume up 
		S->keyControls(key);
		break;
	case 'h':	//[hotel] Volume down
		S->keyControls(key);
		break;
	case 8:	//[backspace] Kill command
		S->keyControls(key);
		break;

	/* We can try to work this out later
	case 'q':
		movement_speed= movement_speed*2;
		break;
	case 'e':
		movement_speed = movement_speed / 2;
		break;
	*/

	}
//checking for pool table sides
	if (!(new_pos.x < 5 | new_pos.x>15 | new_pos.z > 12.5 | new_pos.z < 7.5)) {
		if (new_pos.x > 5 & new_pos.x < 5 + movement_speed*2) {
			new_pos.x = 5.0f;
		}
		else if (new_pos.x < 15 & new_pos.x > 15 - movement_speed*2) {
			new_pos.x = 15.0f;
		}
		else if (new_pos.z > 7.5 & new_pos.z < 7.5 + movement_speed*2) {
			new_pos.z = 7.5f;
		}
		else if (new_pos.z < 12.5 & new_pos.z > 12.5 - movement_speed*2) {
			new_pos.z = 12.5f;
		}
	}
	//checking for player collision
	vec4 myPosition = new_pos;
	U->switchU();
	vec4 otherPosition = (U->getCameraPos());
	U->switchU();

	if ((myPosition.x > otherPosition.x - 2.0f && myPosition.x < otherPosition.x + 2.0f) && (myPosition.z > otherPosition.z - 2.0f && myPosition.z < otherPosition.z + 2.0f)) {
		if (myPosition.x > otherPosition.x + 2.0 - movement_speed * 2) {
			new_pos.x = otherPosition.x + 2.0f;
		}
		else if (myPosition.x < otherPosition.x - 2.0f + movement_speed * 2) {
			new_pos.x = otherPosition.x - 2.0f;
		}
		if (myPosition.z > otherPosition.z + 2.0 - movement_speed * 2) {
			new_pos.z = otherPosition.z + 2.0f;
		}
		else if (myPosition.z < otherPosition.z - 2.0f + movement_speed * 2) {
			new_pos.z = otherPosition.z - 2.0f;
		}
	}
	//its a wall collision
	else if (!(new_pos.x > -4 & new_pos.x < 24 & new_pos.z < 24 & new_pos.z > -4)) {
		if (new_pos.x > 24) {
			new_pos.x = 24.0f;
		}
		else if (new_pos.x < -4) {
			new_pos.x = -4.0f;
		}
		if (new_pos.z > 24) {
			new_pos.z = 24.0f;
		}
		else if (new_pos.z < -4) {
			new_pos.z = -4.0f;
		}
	}

	U->setCameraPos(new_pos);

//checking for walls
	glutPostRedisplay();
};

/****************************************************************************\
 * Callback function when the window is resized                                                           
 * Arguments:
 *     widthi is the new window width in pixels
 *     heighti is the new window height in pixels
\****************************************************************************/
void Resize(GLsizei width, GLsizei height) 
{
	glViewport(10, 10, width-20, height-20); 
	//RenderScene will automatically be called after
}

/****************************************************************************\
 * Main entry point                                                           
 * Arguments:
 *     argc is the number of argument entered at the console
 *     argv is an array of argc pointers to each null-terminated char arrays 
 * Return: 0 if normally terminated
\****************************************************************************/
int main(int argc, char* argv[]) 
{
	std::cout << "Hello World" << std::endl;

	//Init glut
	glutInit(&argc, argv);
	
	//Create a first window (before initializing GLEW)
	glutInitWindowSize(1200, 900);
	glutInitWindowPosition((1920-1200)/2, 100);
	glutCreateWindow("3DLab");

	//Init glew 
	GLenum err = glewInit();
	if (GLEW_OK != err) {
		std::cerr << "GLEW Error: " << glewGetErrorString(err) << "\n";
		return 1;
    }
	
	//Reading opengl version
	const GLubyte* vendor = glGetString(GL_VENDOR);
	std::cout << "OpenGL vendor: " << vendor << std::endl;
	const GLubyte* renderer = glGetString(GL_RENDERER);
	std::cout << "OpenGL renderer: " << vendor << std::endl;
	const GLubyte* ver = glGetString(GL_VERSION);
	std::cout << "OpenGL version: " << ver << std::endl;
	const GLubyte * ver2 = glGetString(GL_SHADING_LANGUAGE_VERSION);
	std::cout << "OpenGL GLSL version: " << ver2 << std::endl;

	//Initialize display mode, double-buffer, RGBA, 3D
	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGBA|GLUT_DEPTH);
	
	myScene.GetEnviObject();
	myScene.GetBallObject();
	myScene.GetCueObject();
	myScene.GetPlayerObject();
	myScene.GetSurfaceObject();

	//Handling events
	glutReshapeFunc(Resize);
    glutDisplayFunc(RenderScene);
	//Create key controller & menu & mouse controller
	glutSpecialFunc(Controller);
	glutKeyboardFunc(Controller2);
	glutPassiveMotionFunc(MouseController);
	glutSetCursor(GLUT_CURSOR_NONE);

	//Load ressources and scene
	myScene.LoadResource();
	U = new Users(vec4(2.0f, 5.0f, 10.0f, 1.0f), vec4(18.0f, 5.0f, 10.0f, 1.0f), 270.0f, 90.0f);
	B = new BallCollection();
	S = new SoundEngine(B, U);
	if (!S->SoundEngine::SoundLoad()) {
		std::cout << "Error loading sounds!" << std::endl;
	}
	else {
		std::cout << "Sounds loaded!" << std::endl;
	}
	//Add the sound engine in the main loop
	S->SoundEngine::SoundEvents(vec3(0.0f, 0.0f, 0.0f), 0);
	P = new Physics(B, S);
	myScene.LoadScene(B, U);

	//Start GLUT event loop
	glutMainLoop();
	
	return 0;
}