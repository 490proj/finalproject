#version 330

in vec4 vVertex;
uniform mat4 viewMat; 

void main(void) {
	gl_Position = viewMat*vVertex;
}
