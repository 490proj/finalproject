
#include "Physics.h"
#include "mat.h"

Physics::Physics(BallCollection* b, SoundEngine* s)
{
	m_b = b;
	m_s = s;
}

Physics::~Physics()
{
}

//Math functions

float HitStrength = 0.006f;
float BallRadius = 0.09375f;
float PocketRadius = 0.36f;

vec4 Physics::vectorSubtraction(const vec4 & u, const vec4 & v)
{
	vec4 r = { u.x - v.x, u.y - v.y, u.z - v.z, u.w - v.w };

	return r;
}


//Physics method


void Physics::CollisionBallVsWall(int i)
{
	vec4 ballPos = m_b->getBallPosition(i);
	vec4 ballVel = m_b->getBallVelocity(i);

	bool OnTable = m_b->returnOnTable(i);

	if (OnTable) {
		if ((ballPos.x + BallRadius >= 14.55f || ballPos.x - BallRadius <= 5.45f) && (ballPos.z + BallRadius <= 11.65f && ballPos.z - BallRadius >= 8.35f)) {
			if (ballPos.x + BallRadius >= 14.0f) {
				m_b->setBallPosition(i, vec4(14.55f - BallRadius, ballPos.y, ballPos.z, ballPos.w));
			}
			if (ballPos.x + BallRadius <= 6.0f) {
				m_b->setBallPosition(i, vec4(5.45f + BallRadius, ballPos.y, ballPos.z, ballPos.w));
			}
			m_b->setBallVelocity(i, vec4(ballVel.x * -1, ballVel.y, ballVel.z, ballVel.w));		m_s->SoundEvents(vec3(ballPos.x, ballPos.y, ballPos.z), 2);
		}
		if ((ballPos.z + BallRadius >= 12.04f || ballPos.z - BallRadius <= 7.96f) && (ballPos.x + BallRadius <= 14.15f && ballPos.x - BallRadius >= 10.35f)) {
			if (ballPos.z + BallRadius >= 11.0f) {
				m_b->setBallPosition(i, vec4(ballPos.x, ballPos.y, 12.04f - BallRadius, ballPos.w));
			}
			if (ballPos.z + BallRadius <= 9.0f) {
				m_b->setBallPosition(i, vec4(ballPos.x, ballPos.y, 7.96f + BallRadius, ballPos.w));
			}
			m_b->setBallVelocity(i, vec4(ballVel.x, ballVel.y, ballVel.z * -1, ballVel.w));		m_s->SoundEvents(vec3(ballPos.x, ballPos.y, ballPos.z), 2);
		}
		if ((ballPos.z + BallRadius >= 12.04f || ballPos.z - BallRadius <= 7.96f) && (ballPos.x + BallRadius <= 9.65f && ballPos.x - BallRadius >= 5.85f)) {
			if (ballPos.z + BallRadius >= 11.0f) {
				m_b->setBallPosition(i, vec4(ballPos.x, ballPos.y, 12.04f - BallRadius, ballPos.w));
			}
			if (ballPos.z + BallRadius <= 9.0f) {
				m_b->setBallPosition(i, vec4(ballPos.x, ballPos.y, 7.96f + BallRadius, ballPos.w));
			}
			m_b->setBallVelocity(i, vec4(ballVel.x, ballVel.y, ballVel.z * -1, ballVel.w));		m_s->SoundEvents(vec3(ballPos.x, ballPos.y, ballPos.z), 2);
		}

		else if (ballPos.x + BallRadius < (0.588235f*ballPos.z + 0.588235f) && ballPos.z + BallRadius < 8.35f) // 1
		{
			float normalX = -0.86193f;
			float normalZ = 0.50702f;
			float t = ballVel.x * normalX + ballVel.z * normalZ;
			float newX = ballVel.x - 2 * t * normalX;
			float newZ = ballVel.z - 2 * t * normalZ;

			m_b->setBallVelocity(i, vec4(newX, ballVel.y, newZ, ballVel.w));	//m_s->SoundEvents(vec3(ballPos.x, ballPos.y, ballPos.z), 2);
		}
		else if (ballPos.x - BallRadius > (1.7f*ballPos.z - 7.75f) && ballPos.z + BallRadius < 8.0f) // 2
		{
			float normalX = -0.50702f;
			float normalZ = 0.86193f;
			float t = ballVel.x * normalX + ballVel.z * normalZ;
			float newX = ballVel.x - 2 * t * normalX;
			float newZ = ballVel.z - 2 * t * normalZ;

			m_b->setBallVelocity(i, vec4(newX, ballVel.y, newZ, ballVel.w));	//m_s->SoundEvents(vec3(ballPos.x, ballPos.y, ballPos.z), 2);
		}
		else if (ballPos.x + BallRadius < (-0.7f*ballPos.z + 15.25f) && ballPos.z + BallRadius < 8.0f) // 3
		{
			float normalX = 0.86193f;
			float normalZ = 0.5734f;
			float t = ballVel.x * normalX + ballVel.z * normalZ;
			float newX = ballVel.x - 2 * t * normalX;
			float newZ = ballVel.z - 2 * t * normalZ;

			m_b->setBallVelocity(i, vec4(newX, ballVel.y, newZ, ballVel.w));		//m_s->SoundEvents(vec3(ballPos.x, ballPos.y, ballPos.z), 2);
		}
		else if (ballPos.x - BallRadius > (0.7f*ballPos.z + 4.75f) && ballPos.z + BallRadius < 8.0f) // 4
		{
			float normalX = -0.81923f;
			float normalZ = 0.57346f;
			float t = ballVel.x * normalX + ballVel.z * normalZ;
			float newX = ballVel.x - 2 * t * normalX;
			float newZ = ballVel.z - 2 * t * normalZ;

			m_b->setBallVelocity(i, vec4(newX, ballVel.y, newZ, ballVel.w));		//m_s->SoundEvents(vec3(ballPos.x, ballPos.y, ballPos.z), 2);
		}
		else if (ballPos.x + BallRadius < (-1.7f*ballPos.z - 27.75f) && ballPos.z + BallRadius < 8.0f) // 5
		{
			float normalX = 0.50702f;
			float normalZ = 0.86193f;
			float t = ballVel.x * normalX + ballVel.z * normalZ;
			float newX = ballVel.x - 2 * t * normalX;
			float newZ = ballVel.z - 2 * t * normalZ;

			m_b->setBallVelocity(i, vec4(newX, ballVel.y, newZ, ballVel.w));		//m_s->SoundEvents(vec3(ballPos.x, ballPos.y, ballPos.z), 2);
		}
		else if (ballPos.x - BallRadius > (-0.58824f*ballPos.z + 19.41176f) && ballPos.z + BallRadius < 8.35f) // 6
		{
			float normalX = 0.86193f;
			float normalZ = 0.50702f;
			float t = ballVel.x * normalX + ballVel.z * normalZ;
			float newX = ballVel.x - 2 * t * normalX;
			float newZ = ballVel.z - 2 * t * normalZ;

			m_b->setBallVelocity(i, vec4(newX, ballVel.y, newZ, ballVel.w));		//m_s->SoundEvents(vec3(ballPos.x, ballPos.y, ballPos.z), 2);
		}
		else if (ballPos.x - BallRadius > (0.588235f*ballPos.z - 7.647059f) && ballPos.z - BallRadius > 12.5f) // 7
		{
			float normalX = -0.86193f;
			float normalZ = 0.50702f;
			float t = ballVel.x * normalX + ballVel.z * normalZ;
			float newX = ballVel.x - 2 * t * normalX;
			float newZ = ballVel.z - 2 * t * normalZ;

			m_b->setBallVelocity(i, vec4(newX, ballVel.y, newZ, ballVel.w));		//m_s->SoundEvents(vec3(ballPos.x, ballPos.y, ballPos.z), 2);
		}
		else if (ballPos.x + BallRadius < (1.7f*ballPos.z - 6.25f) && ballPos.z - BallRadius > 12.0f) // 8
		{
			float normalX = -0.50702f;
			float normalZ = 0.86193f;
			float t = ballVel.x * normalX + ballVel.z * normalZ;
			float newX = ballVel.x - 2 * t * normalX;
			float newZ = ballVel.z - 2 * t * normalZ;

			m_b->setBallVelocity(i, vec4(newX, ballVel.y, newZ, ballVel.w));		//m_s->SoundEvents(vec3(ballPos.x, ballPos.y, ballPos.z), 2);
		}
		else if (ballPos.x - BallRadius > (-0.7f*ballPos.z + 18.75f) && ballPos.z - BallRadius > 12.0f) // 9
		{
			float normalX = 0.81923f;
			float normalZ = 0.57346f;
			float t = ballVel.x * normalX + ballVel.z * normalZ;
			float newX = ballVel.x - 2 * t * normalX;
			float newZ = ballVel.z - 2 * t * normalZ;

			m_b->setBallVelocity(i, vec4(newX, ballVel.y, newZ, ballVel.w));		//m_s->SoundEvents(vec3(ballPos.x, ballPos.y, ballPos.z), 2);
		}
		else if (ballPos.x + BallRadius < (0.7f*ballPos.z + 1.25f) && ballPos.z - BallRadius > 12.0f) // 10
		{
			float normalX = -0.81923f;
			float normalZ = 0.57346f;
			float t = ballVel.x * normalX + ballVel.z * normalZ;
			float newX = ballVel.x - 2 * t * normalX;
			float newZ = ballVel.z - 2 * t * normalZ;

			m_b->setBallVelocity(i, vec4(newX, ballVel.y, newZ, ballVel.w));		//m_s->SoundEvents(vec3(ballPos.x, ballPos.y, ballPos.z), 2);
		}
		else if (ballPos.x - BallRadius > (-1.7f*ballPos.z + 26.25f) && ballPos.z - BallRadius > 12.0f) // 11
		{
			float normalX = 0.50702f;
			float normalZ = 0.86193f;
			float t = ballVel.x * normalX + ballVel.z * normalZ;
			float newX = ballVel.x - 2 * t * normalX;
			float newZ = ballVel.z - 2 * t * normalZ;

			m_b->setBallVelocity(i, vec4(newX, ballVel.y, newZ, ballVel.w));		//m_s->SoundEvents(vec3(ballPos.x, ballPos.y, ballPos.z), 2);
		}
		else if (ballPos.x + BallRadius < (-0.58824f*ballPos.z - 12.35294f) && ballPos.z - BallRadius > 12.5f) // 12
		{
			float normalX = 0.86193f;
			float normalZ = 0.50702f;
			float t = ballVel.x * normalX + ballVel.z * normalZ;
			float newX = ballVel.x - 2 * t * normalX;
			float newZ = ballVel.z - 2 * t * normalZ;

			m_b->setBallVelocity(i, vec4(newX, ballVel.y, newZ, ballVel.w));		//m_s->SoundEvents(vec3(ballPos.x, ballPos.y, ballPos.z), 2);
		}

		if ((ballPos.x - BallRadius < 5.25 | ballPos.x + BallRadius > 14.75 | ballPos.z + BallRadius > 12.25 | ballPos.z - BallRadius < 7.75)) {
			if (ballPos.x - BallRadius < 5.25) {
				m_b->setBallPosition(i, vec4((5.5f + BallRadius), ballPos.y, ballPos.z, ballPos.w));
				m_b->setBallVelocity(i, vec4(ballVel.x * -1, ballVel.y, ballVel.z, ballVel.w));
			}
			else if (ballPos.x + BallRadius > 14.75) {
				m_b->setBallPosition(i, vec4((14.5f - BallRadius), ballPos.y, ballPos.z, ballPos.w));
				m_b->setBallVelocity(i, vec4(ballVel.x * -1, ballVel.y, ballVel.z, ballVel.w));
			}
			else if (ballPos.z - BallRadius < 7.75) {
				m_b->setBallPosition(i, vec4(ballPos.x, ballPos.y, (8.0f + BallRadius), ballPos.w));
				m_b->setBallVelocity(i, vec4(ballVel.x, ballVel.y, ballVel.z * -1, ballVel.w));
			}
			else if (ballPos.z + BallRadius > 12.25) {
				m_b->setBallPosition(i, vec4(ballPos.x, ballPos.y, (12.0f - BallRadius), ballPos.w));
				m_b->setBallVelocity(i, vec4(ballVel.x, ballVel.y, ballVel.z * -1, ballVel.w));
			}
		}
	}
}

bool Physics::PocketCheck(const vec4 & ballPos)
{
	vec4 pockets[]={vec4(5.3f, 0.0f, 12.2f, 0.0f),
					vec4(10.0f, 0.0f, 12.2f, 0.0f),
					vec4(14.7f, 0.0f, 12.2f, 0.0f),
					vec4(5.3f, 0.0f, 7.8f, 0.0f),
					vec4(10.0f, 0.0f, 7.8f, 0.0f),
					vec4(14.7f, 0.0f, 7.8f, 0.0f)
};

	for (int x = 0; x < 6; x++) {
		vec4 distance = vectorSubtraction(ballPos, pockets[x]);
		float distanceMagnitude = sqrt(pow(distance.x, 2) + pow(distance.z, 2));

		if (distanceMagnitude <= PocketRadius) {
			return true;
		}
	}
	return false;
}

bool Physics::CheckIfBallIsMoving(const vec4 & ballVelocity)
{
	float speed = sqrt(pow(ballVelocity.x, 2) + pow(ballVelocity.y, 2));

	if (speed != 0.0f) { return true; }
	else { return false; };
}

bool Physics::MovementCheck()
{
	int BallCounter = 0;
	for (int x = 0; x < 16; x++) {
		vec4 BallVelocity = m_b->getBallVelocity(x);
		if (CheckIfBallIsMoving(BallVelocity)) { BallCounter++; };
	}
	if (BallCounter == 0) { return false; }
	else { return true; };
}

void Physics::Sink(const int number)
{
	vec4 BallVelocity = { 0.0f, -0.004f, 0.0f, 0.0f };
	m_b->setBallVelocity(number, BallVelocity);

	vec4 BallPosition = m_b->getBallPosition(number);

	if (BallPosition.y <= 2.50f) {
		BallVelocity = { 0.0f, 0.0f, 0.0f, 0.0f };
		m_b->setBallVelocity(number, BallVelocity);
		m_b->falseOnTable(number);
		m_b->setBallPosition(number, vec4(130.0f, 0.0f, 0.0f, 0.0f));
		if (number != 0) {
			m_s->SoundEvents(vec3(BallPosition.x, BallPosition.y, BallPosition.z), 4);
		}
		else {
			m_s->SoundEvents(vec3(BallPosition.x, BallPosition.y, BallPosition.z), 8);
		}
		
	}
}

vec4 Physics::Friction(const vec4 & ballVelocity)
{
	float FrictionCoefficient = 0.003f;
	vec4 newMovementVector = { ballVelocity.x * (1 - FrictionCoefficient), 0.0f, ballVelocity.z * (1 - FrictionCoefficient), 0.0f };

	if (sqrt(pow(newMovementVector.x, 2) + pow(newMovementVector.z, 2)) <= 0.0001) { newMovementVector = { 0.0f, 0.0f, 0.0f, 0.0f }; };

	return newMovementVector;
}

vec4 Physics::BallMovement(const vec4 & ballPos, const vec4 & ballVelocity)
{
	vec4 newPosition = { (ballPos.x + ballVelocity.x), (ballPos.y + ballVelocity.y), (ballPos.z + ballVelocity.z), ballPos.w };

	return newPosition;
}

vec4 Physics::TurnBall(const vec4 &ballRot, const vec4 &ballVel) 
{
	vec4 newRotation = { (ballRot.x + 360*ballVel.z), (ballRot.y + ballVel.y), (ballRot.z + 360*ballVel.x), ballRot.w };

	return newRotation;
}

void Physics::SetStrength(const char & key)
{
	float BaseStrength = 0.008f;
	if (key == '1') { HitStrength = 1 * BaseStrength; };
	if (key == '2') { HitStrength = 2 * BaseStrength; };
	if (key == '3') { HitStrength = 3 * BaseStrength; };
	if (key == '4') { HitStrength = 4 * BaseStrength; };
	if (key == '5') { HitStrength = 5 * BaseStrength; };
}

vec4 Physics::BallInitialMovement(const vec4 & cameraPos, const vec4 & ballPos)
{
	vec4 direction = { ballPos.x - cameraPos.x, 0.0f, ballPos.z - cameraPos.z, 0.0f };

	float magnitude = sqrt(pow(direction.x, 2) + pow(direction.z, 2));
	vec4 unitVector = { (direction.x / magnitude), 0.0f, (direction.z / magnitude), 0.0f };

	vec4 newMovementVector = { (unitVector.x * HitStrength), 0.0f, (unitVector.z * HitStrength), 0.0f };

	return newMovementVector;
}


bool Physics::DoCirclesOverlap(int ball1, int ball2)
{
	vec4 ballPos1 = m_b->getBallPosition(ball1);
	vec4 ballPos2 = m_b->getBallPosition(ball2);
	return sqrt((ballPos1.x - ballPos2.x)*(ballPos1.x - ballPos2.x) + (ballPos1.z - ballPos2.z)*(ballPos1.z - ballPos2.z)) < (BallRadius * 2);
}


void Physics::update()
{
	bool CollidingPairs[16][16];

	for (int i = 0; i < 16; i++)
	{
		vec4 ballPos = m_b->getBallPosition(i);
		vec4 ballVel = m_b->getBallVelocity(i);

		bool pocketCheck = PocketCheck(ballPos);
		if (pocketCheck && m_b->returnOnTable(i) == true) {
			Sink(i);
		}
		CollisionBallVsWall(i);
	}

	// Update Ball Positions
	for(int i = 0; i < 16; i++)
	{
		vec4 ballPos = m_b->getBallPosition(i);
		vec4 ballVel = m_b->getBallVelocity(i);
		vec4 ballRot = m_b->getBallRotation(i);

		m_b->setBallPosition(i ,BallMovement(ballPos, ballVel));
		//m_s->SoundEvents(vec3(ballPos.x, ballPos.y, ballPos.z), 5);

		m_b->setBallVelocity(i, Friction(ballVel));

		m_b->setBallRotation(i, TurnBall(ballRot, ballVel));


		// Clamp velocity near zero
		if (fabs(ballVel.x*ballVel.x + ballVel.z*ballVel.z) < 0.0000001f)
		{
			m_b->setBallVelocity(i, (0.0f, 0.0f, 0.0f, 0.0f));
		}
	}
		
	// Static collisions, i.e. overlap
	for(int i = 0; i < 16; i++)
	{
		for (int j = 0; j < 16; j++)
		{
			bool OnTableI = m_b->returnOnTable(i);
			bool OnTableJ = m_b->returnOnTable(j);

			if (i != j && OnTableI && OnTableJ)
			{
				if (DoCirclesOverlap(i, j))
				{
					// Collision has occured
					CollidingPairs[i][j] = true;

					vec4 ballPos = m_b->getBallPosition(i);
					vec4 ballPos2 = m_b->getBallPosition(j);

					// Distance between ball centers
					float fDistance = sqrt((ballPos.x - ballPos2.x)*(ballPos.x - ballPos2.x) + (ballPos.z - ballPos2.z)*(ballPos.z - ballPos.z));

					// Calculate displacement required
					float fOverlap = ((BallRadius * 2) - fDistance);

					// Displace Current Ball away from collision

					ballPos.x += (fOverlap * (ballPos.x - ballPos2.x) / (BallRadius * 2));
					ballPos.z += (fOverlap * (ballPos.z - ballPos2.z) / (BallRadius * 2));
					m_b->setBallPosition(i, ballPos);

					// Sound
					m_s->SoundEvents(vec3(ballPos.x, ballPos.y, ballPos.z), 1);
				}
			}
		}
	}
		
	// Now work out dynamic collisions
	for (int i = 0; i < 16; i++)
	{
		for (int j = 0; j < 16; j++)
		{
			if (j > i && CollidingPairs[i][j] == true)
			{
				vec4 ballPos = m_b->getBallPosition(i);
				vec4 ballVel = m_b->getBallVelocity(i);
				vec4 ballPos2 = m_b->getBallPosition(j);
				vec4 ballVel2 = m_b->getBallVelocity(j);

				// Distance between balls
				float fDistance = sqrtf((ballPos.x - ballPos2.x)*(ballPos.x - ballPos2.x) + (ballPos.z - ballPos2.z)*(ballPos.z - ballPos2.z));

				// Normal
				float nx = (ballPos2.x - ballPos.x) / fDistance;
				float ny = (ballPos2.z - ballPos.z) / fDistance;

				// Tangent
				float tx = -ny;
				float ty = nx;

				// Dot Product Tangent
				float dpTan1 = ballVel.x * tx + ballVel.z * ty;
				float dpTan2 = ballVel2.x * tx + ballVel2.z * ty;

				// Dot Product Normal
				float dpNorm1 = ballVel.x * nx + ballVel.z * ny;
				float dpNorm2 = ballVel2.x * nx + ballVel2.z * ny;

				// Update ball velocities
				ballVel.x = tx * dpTan1 + nx * dpNorm2;
				ballVel.z = ty * dpTan1 + ny * dpNorm2;
				ballVel2.x = tx * dpTan2 + nx * dpNorm1;
				ballVel2.z = ty * dpTan2 + ny * dpNorm1;
				m_b->setBallVelocity(i, 0.9f*ballVel);
				m_b->setBallVelocity(j, 0.9f*ballVel2);
			}

		}
	}
}