
#include <iostream>
#include <fstream>

#include "Angel.h"
#include "LoadBMP.h"

namespace Rivest {

/****************************************************************************\
 * Load a BMP file into memory                                                          
 * Arguments:
 *     filename  the name of the .BMP file
 *     width     used to return the width of the image
 *     height    used to return the height of the image
 * Return:
 *     A pointer to a newly allocated array for the image bytes
 *     or nullptr if any error occured.
\****************************************************************************/
unsigned char* loadBMP(const char *filename, int& width, int& height) 
{
	
	//Open the file (on stack)
	std::ifstream inFile(filename, std::ios_base::binary);
	if (!inFile.good()) {
		std::cerr << "Error opening file " << filename << std::endl;
		return nullptr;
	}

	//Read the file header (on stack)
	BITMAPFILEHEADER fileHeader;
	//Note that this way of reading headers is not portable to non-intel (or not little-endian) processors 
	//Intel processors are putting multi-bytes numbers in reverse order of significance (as in BMP files)
	inFile.read((char*) (&fileHeader), sizeof(fileHeader));
	if (!inFile.good()) {
		std::cerr << "Error reading BITMAPFILEHEADER!" << std::endl;
		return nullptr;
	} 
	//Check signature
	if ((fileHeader.signature[0] != 'B') || (fileHeader.signature[1] != 'M')) {
		std::cerr << "Error reading BM signature!" << std::endl;
		return nullptr;
	}
	//Output information
	std::cout << "Fileze size is " << fileHeader.filesize << " bytes" << std::endl;
	std::cout << "Data in file start at position " << fileHeader.offset << " bytes" << std::endl;
	//Check if we can deal with header size
	if (fileHeader.offset != 54) {
		std::cerr << "Error, unsupported DIB header!" << std::endl;
		return nullptr;
	}

	//Read the DIB header (on stack)
	BITMAPINFOHEADER DIBHeader;
	//Note that this way of reading headers is not portable to non-intel (or not little-endian) processors 
	//Intel processors are putting multi-bytes numbers in reverse order of significance (as in BMP files)
	inFile.read((char*)(&DIBHeader), sizeof(DIBHeader));
	if (!inFile.good()) {
		std::cerr << "Error reading BITMAPV5HEADER!" << std::endl;
		return nullptr;
	}
	//Check if we can deal with header size
	if (DIBHeader.headerSize != 40) {
		std::cerr << "Error, unsupported DIB header!" << std::endl;
		return nullptr;
	}
	//Output and check information
	std::cout << "Image width = " << DIBHeader.width << " pixels" << std::endl;
	//Check that width is a multiple of 4, otherwise we need to deal with padding
	/*if ((DIBHeader.width % 4) != 0) {
		std::cerr << "Error, width not a multiple of 4 not supported!" << std::endl;
		return nullptr;
	}*/
	std::cout << "Image height = " << DIBHeader.height << " pixels" << std::endl;
	if (DIBHeader.planeCount != 1) {
		std::cerr << "Error reading plane count!" << std::endl;
		return nullptr;
	}
	std::cout << "Color depth = " << DIBHeader.bitsPerPixel << " bits per pixel" << std::endl;
	if (DIBHeader.bitsPerPixel != 24) {
		std::cerr << "Error reading plane count!" << std::endl;
		return nullptr;
	}
	if (DIBHeader.planeCount != 1) {
		std::cerr << "Error, not reading plane count!" << std::endl;
		return nullptr;
	}
	if (DIBHeader.compressionMode != 0) {
		std::cerr << "Error, unsupported compression mode!" << std::endl;
		return nullptr;
	}
	std::cout << "Image size = " << DIBHeader.rawSize << " bytes" << std::endl;
	//Image size if usually set to 0 with no compression, so let's make it right for our 24bpp images
	DIBHeader.rawSize = DIBHeader.width*DIBHeader.height * 3;
	std::cout << "Corrected image size = " << DIBHeader.rawSize << " bytes" << std::endl;
	std::cout << "Horizontal resolution = " << DIBHeader.horzResolution << " pixels per meter" << std::endl;
	std::cout << "Vertical resolution = " << DIBHeader.vertResolution << " pixels per meter" << std::endl;
	if (DIBHeader.paletteColorCount != 0) {
		std::cerr << "Error, unsupported color palette!" << std::endl;
		return nullptr;
	}

	//If we get there, we are ready to load the data
	
	//Make space for data (on heap)
	unsigned char* data = new unsigned char[DIBHeader.rawSize];
	if (data == nullptr) {
		std::cerr << "Error, not enought memory!" << std::endl;
		return nullptr;
	}

	//Read data (on heap)
	inFile.read((char *) data, DIBHeader.rawSize);
	if (!inFile.good()) {
		std::cerr << "Error reading image data!" << std::endl;
		delete[] data; //(clean the heap memory)
		return nullptr;
	} 

	//No clean up needed, on stack objects are automatically closed and destroyed properly
	std::cout << "Image loaded successfully!" << std::endl;
	width = DIBHeader.width;
	height = DIBHeader.height;
	return data; //But the caller will be responsible of deleting the data from heap memory

}

/****************************************************************************\
 * Loading texture from a BMP file into GPU ram
 *     filename  the name of the .BMP file
 * Return:
 *     A valid gl texturID or 0 if an error occured.
\****************************************************************************/
GLuint  LoadTexFromBMP(const char* filename) {

	//Output
	std::cout << "Loading..." << filename << std::endl;

	//Load bmp
	int width, height;
	unsigned char* data = loadBMP(filename, width, height);
	//Check if read properly
	if (data == nullptr) {
		return 0;
	}

	GLuint texID;
	//Ask for space for a texture
	glGenTextures(1, &texID);
	//Make it the current texture
	glBindTexture(GL_TEXTURE_2D, texID);

	// Send the image to OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);
	std::cout << filename << " texture ID = " << texID << std::endl; 

	//Set some filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	//Clear memory for the image on CPU heap
	delete[] data;

	return texID;
}

}