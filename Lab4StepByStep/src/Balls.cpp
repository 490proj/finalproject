
#include "Balls.h"
#include <assert.h>

BallCollection::BallCollection()
{
	for (int i = 0; i < 16; i++)
	{
		m_Balls[i].ballNumber = i;
	}
	Reset();
}

void BallCollection::Reset()
{
	for (int i = 0; i < 16; i++)
	{
		m_Balls[i].velocity = vec4(0.0f, 0.0f, 0.0f, 0.0f);
		m_Balls[i].rotation = vec4(0.0f, 0.0f, 0.0f, 0.0f);
		m_Balls[i].onTable = true;
	}
	setBallPosition(0, vec4(10.0f - 2.36f, 2.75f + 0.1875f/2.0f, 10.0f, 0.0f));

	setBallPosition(1, vec4(10.0f + 2.36f, 2.75f + 0.1875f / 2.0f, 10.0f, 0.0f));
	setBallPosition(2, vec4(10.0f + 2.36f + 0.1629f * 4, 2.75f + 0.1875f / 2.0f, 10.0f + 0.18765f * 2, 0.0f));
	setBallPosition(3, vec4(10.0f + 2.36f + 0.1629f * 4, 2.75f + 0.1875f / 2.0f, 10.0f - 0.18765f * 2, 0.0f));
	setBallPosition(4, vec4(10.0f + 2.36f + 0.1629f * 2, 2.75f + 0.1875f / 2.0f, 10.0f + 0.18765f, 0.0f));
	setBallPosition(5, vec4(10.0f + 2.36f + 0.1629f * 2, 2.75f + 0.1875f / 2.0f, 10.0f - 0.18765f, 0.0f));
	setBallPosition(6, vec4(10.0f + 2.36f + 0.1629f * 3, 2.75f + 0.1875f / 2.0f, 10.0f + 0.18765f / 2, 0.0f));
	setBallPosition(7, vec4(10.0f + 2.36f + 0.1629f * 4, 2.75f + 0.1875f / 2.0f, 10.0f - 0.18765f, 0.0f));
	setBallPosition(8, vec4(10.0f + 2.36f + 0.1629f * 2, 2.75f + 0.1875f / 2.0f, 10.0f, 0.0f));
	setBallPosition(9, vec4(10.0f + 2.36f + 0.1629f * 3, 2.75f + 0.1875f / 2.0f, 10.0f - 0.18765f / 2, 0.0f));
	setBallPosition(10, vec4(10.0f + 2.36f + 0.1629f, 2.75f + 0.1875f / 2.0f, 10.0f - 0.18765f / 2, 0.0f));
	setBallPosition(11, vec4(10.0f + 2.36f + 0.1629f, 2.75f + 0.1875f / 2.0f, 10.0f + 0.18765f / 2, 0.0f));
	setBallPosition(12, vec4(10.0f + 2.36f + 0.1629f * 4, 2.75f + 0.1875f / 2.0f, 10.0f, 0.0f));
	setBallPosition(13, vec4(10.0f + 2.36f + 0.1629f * 3, 2.75f + 0.1875f / 2.0f, 10.0f + 0.18765f * 3 / 2, 0.0f));
	setBallPosition(14, vec4(10.0f + 2.36f + 0.1629f * 3, 2.75f + 0.1875f / 2.0f, 10.0f - 0.18765f * 3 / 2, 0.0f));
	setBallPosition(15, vec4(10.0f + 2.36f + 0.1629f * 4, 2.75f + 0.1875f / 2.0f, 10.0f + 0.18765f, 0.0f));
}

Ball* BallCollection::operator[](int index) 
{
	assert(index>=0 && "The ball number you asked for is smaller than zero!");
	assert(index<=15 && "You cannot ask for a ball number greater than 15!");
	return &m_Balls[index];
}

vec4 BallCollection::getBallPosition(int number)
{
	assert(number >= 0 && "The ball number you asked for is smaller than zero!");
	assert(number <= 15 && "You cannot ask for a ball number greater than 15!");
	return m_Balls[number].position;
}

vec4 BallCollection::getBallVelocity(int number)
{
	assert(number >= 0 && "The ball number you asked for is smaller than zero!");
	assert(number <= 15 && "You cannot ask for a ball number greater than 15!");
	return m_Balls[number].velocity;
}

vec4 BallCollection::getBallRotation(int number)
{
	assert(number >= 0 && "The ball number you asked for is smaller than zero!");
	assert(number <= 15 && "You cannot ask for a ball number greater than 15!");
	return m_Balls[number].rotation;
}


void BallCollection::setBallPosition(int number, vec4 newPos)
{
	assert(number >= 0 && "The ball number you asked for is smaller than zero!");
	assert(number <= 15 && "You cannot ask for a ball number greater than 15!");
	m_Balls[number].position = newPos;
}

void BallCollection::setBallVelocity(int number, vec4 newVel)
{
	assert(number >= 0 && "The ball number you asked for is smaller than zero!");
	assert(number <= 15 && "You cannot ask for a ball number greater than 15!");
	m_Balls[number].velocity = newVel;
}

void BallCollection::setBallRotation(int number, vec4 newRot)
{
	assert(number >= 0 && "The ball number you asked for is smaller than zero!");
	assert(number <= 15 && "You cannot ask for a ball number greater than 15!");
	m_Balls[number].rotation = newRot;
}

void BallCollection::trueOnTable(int number)
{
	m_Balls[number].onTable = true;
}

void BallCollection::falseOnTable(int number)
{
	m_Balls[number].onTable = false;
}

bool BallCollection::returnOnTable(int number)
{
	return m_Balls[number].onTable;
}