
#include "UserDetail.h"
#include <assert.h>

Users::Users(vec4 cameraPos1, vec4 cameraPos2, float camera_angle_yaw1, float camera_angle_yaw2)
{
	m_Users[0].camera_pos = cameraPos1;
	m_Users[1].camera_pos = cameraPos2;
	m_Users[0].camera_angle_yaw = camera_angle_yaw1;
	m_Users[1].camera_angle_yaw = camera_angle_yaw2;
	m_Users[0].camera_angle_pitch = 0.0f;
	m_Users[1].camera_angle_pitch = 0.0f;
	m_Users[0].movement_speed = MOVEMENT_SPEED;
	m_Users[1].movement_speed = MOVEMENT_SPEED;
	m_Users[0].crouch = false;
	m_Users[1].crouch = false;
	m_currentUser = 0;
};

UserDetail* Users::operator[](int index)
{
	assert(index >= 0 && "The ball number you asked for is smaller than zero!");
	assert(index <= 1 && "You cannot ask for a ball number greater than 15!");
	return &m_Users[index];
}

int Users::getCurrentUser()
{
	return m_currentUser;
}

void Users::setCurrentUser(int newUser)
{
	assert(newUser >= 0 && "Current User cannot be smaller than 0!");
	assert(newUser <= 1 && "Current User cannot be bigger than 1!");
	m_currentUser = newUser;
}

void Users::switchU () {
	{
		if (m_currentUser == 0)
		{
			m_currentUser = 1;
		}
		else
		{
			m_currentUser = 0;
		}
	}
}

void Users::switchCurrentUser()
{
	if (m_currentUser == 0)
	{
		m_Users[m_currentUser].camera_angle_yaw = 270.0f;
		m_Users[m_currentUser].camera_angle_pitch = 0.0f;
		m_Users[m_currentUser].camera_pos = vec4(2.0f, 5.0f, 10.0f, 1.0f);
		m_currentUser = 1;
	}
	else
	{
		m_Users[m_currentUser].camera_angle_yaw = 90.0f;
		m_Users[m_currentUser].camera_angle_pitch = 0.0f;
		m_Users[m_currentUser].camera_pos = vec4(18.0f, 5.0f, 10.0f, 1.0f);
		m_currentUser = 0;
	}
}

vec4 Users::toggleCrouch()
{
	if (m_Users[m_currentUser].crouch == true)
	{
		m_Users[m_currentUser].movement_speed = MOVEMENT_SPEED;
		m_Users[m_currentUser].crouch = false;
		return (m_Users[m_currentUser].camera_pos + vec4{ 0.0f, 1.5f, 0.0f, 0.0f });
	}
	else
	{
		m_Users[m_currentUser].movement_speed = MOVEMENT_SPEED /3.0f;
		m_Users[m_currentUser].crouch = true;
		return (m_Users[m_currentUser].camera_pos - vec4{ 0.0f, 1.5f, 0.0f, 0.0f });
	}
}

float Users::getMovSpeed()
{
	return m_Users[m_currentUser].movement_speed;
}

vec4 Users::getCameraPos()
{
	return m_Users[m_currentUser].camera_pos;
}

float Users::getCameraAngleYaw()
{
	return m_Users[m_currentUser].camera_angle_yaw;
}

float Users::getCameraAnglePitch()
{
	return m_Users[m_currentUser].camera_angle_pitch;
}

void Users::setCameraPos(vec4 entry)
{
	m_Users[m_currentUser].camera_pos = entry;
}

void Users::setCameraAngleYaw(float entry)
{
	m_Users[m_currentUser].camera_angle_yaw = entry;
}

void Users::setCameraAnglePitch(float entry)
{
	m_Users[m_currentUser].camera_angle_pitch = entry;
}